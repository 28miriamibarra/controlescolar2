﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class AlumnoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public AlumnoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Alumno alumno)
        {
            if (alumno.Idalumno == 0)
            {
                string consulta = string.Format("Insert into alumno values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')",
                alumno.Numerocontrol, alumno.Nombre, alumno.Apellidopaterno, alumno.Apellidomaterno, alumno.Fechanac, alumno.Domicilio, alumno.Email, alumno.Sexo,alumno.Estado,alumno.Ciudad);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update alumno set nombre ='{0}', apellidopaterno ='{1}',apellidomaterno='{2}', fechanac='{3}',domicilio='{4}',email='{5}',sexo='{6}',numerocontrol='{7}',fkcodigoestado='{8}',fkciudad='{9}' where idalumno={10}",
                 alumno.Nombre, alumno.Apellidopaterno, alumno.Apellidomaterno, alumno.Fechanac, alumno.Domicilio, alumno.Email, alumno.Sexo,alumno.Numerocontrol,alumno.Estado,alumno.Ciudad,alumno.Idalumno);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idalumno)
        {
            string consulta = string.Format("Delete from alumno where idalumno={0}", idalumno);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Alumno> GetAlumnos(string filtro)
        {
            
            var listAlumno = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "Select * from alumno where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "alumno");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumno
                {
                    Idalumno = Convert.ToInt32(row["idalumno"]),
                    Numerocontrol = Convert.ToInt32(row["numerocontrol"]),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Fechanac = DateTime.Parse(row["fechanac"].ToString()),
                    Domicilio = row["domicilio"].ToString(),
                    Email = row["email"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Estado = row["fkcodigoestado"].ToString(),
                    Ciudad = row["fkciudad"].ToString()

                };

                listAlumno.Add(alumno);
            }

            return listAlumno;
        }

        public List<Alumno> GetNombreAlumno(ComboBox cm)

        {
            var listProfesores = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "select nombre from alumno";
            ds = conexion.ObtenerDatos(consulta, "alumno");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombre";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Alumno
                {
                    Nombre = row["nombre"].ToString()
                };
                listProfesores.Add(profesores);
            }
            return listProfesores;
        }


    }
}
