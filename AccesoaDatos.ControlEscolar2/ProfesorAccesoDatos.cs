﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;
using System.Xml;

namespace AccesoaDatos.ControlEscolar2
{
    public class ProfesorAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ProfesorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Profesores profesor)
        {
            if (profesor.Llave == 0)
            {
                string consulta = string.Format("insert into profesores values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')",
                profesor.NumeroControlMaestro,profesor.Nombre,profesor.ApellidoPaterno,profesor.ApellidoMaterno,profesor.Direccion,profesor.FechaNac,profesor.Ciudad,profesor.Estado,profesor.NumeroCedula,profesor.Titulo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update profesores set numeroControlMaestro ='{1}', nombre ='{2}',apellidoPaterno='{3}', apellidoMaterno='{4}',direccion='{5}',fechanac='{6}',ciudad='{7}',estado='{8}',numeroCedula='{9}',titulo='{10}' where llave = {11}",
                profesor.NumeroControlMaestro, profesor.Nombre, profesor.ApellidoPaterno, profesor.ApellidoMaterno, profesor.Direccion, profesor.FechaNac, profesor.Ciudad, profesor.Estado, profesor.NumeroCedula, profesor.Titulo, profesor.Llave);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(string llave)
        {
            string consulta = string.Format("Delete from profesores where llave={0}", llave);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Profesores> GetProfesores(string filtro)
        {

            var listProfesores = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "Select * from profesores where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesores");

            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var profesor = new Profesores
                {
                    Llave = Convert.ToInt32(row["llave"]),                   
                    NumeroControlMaestro = row["numeroControlMaestro"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    ApellidoPaterno = row["apellidoPaterno"].ToString(),
                    ApellidoMaterno = row["apellidoMaterno"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    FechaNac = DateTime.Parse(row["fechanac"].ToString()),
                    Ciudad = row["ciudad"].ToString(),
                    Estado = row["estado"].ToString(),                   
                    NumeroCedula = Convert.ToInt32(row["numeroCedula"]),
                    Titulo = row["titulo"].ToString()
                };

                listProfesores.Add(profesor);
            }

            return listProfesores;
        }
        

        public DataTable ultimoNumero(string filtro)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select max(substring(numeroControlMaestro,6)+1) as NextId from profesores where numeroControlMaestro like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesores");
            dt = ds.Tables[0];
            return dt;
        }

        public DataSet traerUltimoNumero(string filtro)
        {

            var ds= new DataSet();
             
            //var num = new DataSet("select max(substring(numeroControlMaestro,6)) as NextIncremento from profesores where numeroControlMaestro like '%" + filtro + "%'");
            string consulta = "select max(substring(numeroControlMaestro,6)+1) as NextIncremento from profesores where numeroControlMaestro   like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta,"profesores");
            return ds;

        }

        public List<Profesores> GetProfesoresLists(ComboBox cm)

        {
            var listProfesores = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "select nombre from profesores";
            ds = conexion.ObtenerDatos(consulta, "profesores");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombre";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Profesores
                {
                    Nombre = row["nombre"].ToString()
                };
                listProfesores.Add(profesores);
            }
            return listProfesores;
        }

        public List<Profesores> GetProfesoresID(string filtro)

        {
            var listProfesores = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "Select llave from profesores where nombre  like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesores");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Profesores
                {
                    Llave = Convert.ToInt32(row["llave"].ToString()),
                    
                };

                listProfesores.Add(profesores);
            }

            return listProfesores;
        }


    }
}
