﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;


namespace AccesoaDatos.ControlEscolar2
{
    public class UsuarioAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public UsuarioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        


        public void Guardar(Usuario ususario)
        {
            if (ususario.Idusuario == 0)
            {
                string consulta = string.Format("Insert into ususario values(null,'{0}','{1}','{2}','{3}')", ususario.Nombre, ususario.App, ususario.Apm, ususario.Contrasenia);
               conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update ususario set nombre = '{0}', apellidopaterno ='{1}',apellidomaterno='{2}', contrasenia='{3}'", ususario.Nombre, ususario.App, ususario.Apm, ususario.Contrasenia);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idusuario)
        {
            string consulta = string.Format("Delete from ususario where idusuario={0}", idusuario);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listUsuario = new List<Usuario>();
            var ds = new DataSet();
            string consulta = "Select * from ususario where nombre like '%"+filtro+"%'";
            ds = conexion.ObtenerDatos(consulta, "ususario");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var ususario = new Usuario
                {
                    Idusuario = Convert.ToInt32(row["idUsuario"]),
                    Nombre = row["nombre"].ToString(),
                    App = row["apellidopaterno"].ToString(),
                    Apm = row["apellidomaterno"].ToString(),
                    Contrasenia = row["contrasenia"].ToString()
                };

                listUsuario.Add(ususario);
            }

            
            return listUsuario;
        }

        public DataTable tarerUsuarios()
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select*from ususario";
            ds = conexion.ObtenerDatos(consulta, "ususario");
            dt = ds.Tables[0];
            return dt;
        }
    }
}


