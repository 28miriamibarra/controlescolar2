﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;


namespace AccesoaDatos.ControlEscolar2
{
    public class MateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public MateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Materias materias)
        {
            
            if (materias.Llave == 0)
            {
                string consulta = string.Format("insert into materias values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                  materias.Idmateria, materias.Nombremateria, materias.Numhrsteoria, materias.Numhrspractica, materias.Creditos, materias.Semestre,materias.Materiaant,materias.Materiasig);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update materias set idmateria='{0}', nombremateria ='{1}', numhrsteoria='{2}', numhrspractica='{3}', creditos='{4}', semestre='{5}', materiaant='{6}', materiasig='{7}'  where llave={8}",
                 materias.Idmateria,materias.Nombremateria, materias.Numhrsteoria, materias.Numhrspractica, materias.Creditos, materias.Semestre, materias.Materiaant,materias.Materiasig, materias.Llave);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(string llave)
        {
            string consulta = string.Format("Delete from materias where llave={0}", llave);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Materias> GetMaterias(string filtro)
        {

            var listMaterias = new List<Materias>();
            var ds = new DataSet();
            string consulta = "Select * from materias where nombremateria like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materias");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var materias = new Materias
                {       
                    Llave = Convert.ToInt32(row["llave"].ToString()),
                    Idmateria = row["idmateria"].ToString(),
                    Nombremateria = row["nombremateria"].ToString(),                   
                    Numhrsteoria = Convert.ToInt32(row["numhrsteoria"].ToString()),
                    Numhrspractica = Convert.ToInt32(row["numhrspractica"].ToString()),
                    Creditos =Convert.ToInt32( row["creditos"].ToString()),
                    Semestre = row["semestre"].ToString(),
                    Materiaant = row["materiaant"].ToString(),
                    Materiasig = row["materiasig"].ToString(),

                };

                listMaterias.Add(materias);
            }

            return listMaterias;
        }


        public List<Materias> GetMateriasnom(ComboBox cm)

        {
            var listProfesores = new List<Materias>();
            var ds = new DataSet();
            string consulta = "select nombremateria from materias";
            ds = conexion.ObtenerDatos(consulta, "materias");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombremateria";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Materias
                {
                    Nombremateria = row["nombremateria"].ToString()
                };
                listProfesores.Add(profesores);
            }
            return listProfesores;
        }
    }
}
