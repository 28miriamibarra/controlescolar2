﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class CiudadesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public CiudadesAccesoDatos()
        {
            conexion= new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<Ciudades> GetCiudadesList (string estado, ComboBox cm)
        {
            var listCiudades = new List<Ciudades>();
            var ds = new DataSet();
            string consulta = "select nombre from ciudades where fk_estado = '" + estado + "'";
            ds = conexion.ObtenerDatos(consulta, "ciudades");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombre";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Ciudades
                {
                    Nombreciu = row["nombre"].ToString()

                 };
                 listCiudades.Add(ciudades);
            }
            return listCiudades;
        
        }



        
    }




}

