﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class CalificacionesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public CalificacionesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
           
        }

        public void Guardar(Calificaciones calificaciones)
        {
            if (calificaciones.Idcal == 0)
            {
                string consulta = string.Format("Insert into parciales values(null,'{0}','{1}','{2}','{3}','{4}','{5}')",
                calificaciones.Parcial1,calificaciones.Parcial2, calificaciones.Parcial3, calificaciones.Parcial4, calificaciones.Fkmateria,calificaciones.Fkalumno);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update parciales set Parcial1 ='{0}', Parcial2 ='{1}',Parcial3='{2}', Parcial4='{3}',fkmateria='{4}',fkalumno='{5}' where idcal={6}",
                calificaciones.Parcial1, calificaciones.Parcial2, calificaciones.Parcial3, calificaciones.Parcial4, calificaciones.Fkmateria, calificaciones.Fkalumno, calificaciones.Idcal);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idmateria)
        {
            string consulta = string.Format("Delete from parciales where idcal={0}", idmateria);
            conexion.EjecutarConsulta(consulta);
        }

     
        public List<Alumno> getidalumno(string nombre, ComboBox cm)
        {
            var listCiudades = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "Select idalumno from alumno where nombre = '" + nombre + "'";
            ds = conexion.ObtenerDatos(consulta, "alumno");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "idalumno";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Alumno
                {
                    Idalumno = Convert.ToInt32(row["idalumno"].ToString())
                };
                listCiudades.Add(ciudades);
            }
            return listCiudades;

        }

        public List<AsignarMateria> GetIDList(string nombre, ComboBox cm)
        {
            var listCiudades = new List<AsignarMateria>();
            var ds = new DataSet();
            string consulta = "Select idasigmateria from asignarmateria where materia = '" + nombre + "'";
            ds = conexion.ObtenerDatos(consulta, "asignarmateria");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "idasigmateria";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new AsignarMateria
                {
                    Idasigmateria = Convert.ToInt32(row["idasigmateria"].ToString())
                };
                listCiudades.Add(ciudades);
            }
            return listCiudades;

        }


        /*public List<Calificaciones> GetCalificaciones(string filtro)
        {
            
            var listEstudios = new List<Calificaciones>();
            var ds = new DataSet();
            string consulta = "select*from parciales";

            ds = conexion.ObtenerDatos(consulta, "parciales");


            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var estudios = new Calificaciones
                {

                    Idcal = Convert.ToInt32(row["idestudio"]),
                    Parcial1 = Convert.ToInt32(row["idestudio"]),
                    Parcial2 = Convert.ToInt32(row["idestudio"]),
                    Parcial3 = Convert.ToInt32(row["idestudio"]),
                    Parcial4 = Convert.ToInt32(row["idestudio"]),
                    Fkmateria = Convert.ToInt32(row["idestudio"]),
                    Fkalumno = Convert.ToInt32(row["idestudio"]),
                };

                listEstudios.Add(estudios);
            }


            return listEstudios;
        }*/

    }
}
