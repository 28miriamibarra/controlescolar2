﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;


namespace AccesoaDatos.ControlEscolar2
{
    public class VistaCalificacionesAccesoDatos
    {       
        VistaCalificaciones _vistaCalificaciones = new VistaCalificaciones();
        ConexionAccesoDatos conexion;
        

        public VistaCalificacionesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<VistaCalificaciones> GetVistas(string filtro)
        {

            var listVistas = new List<VistaCalificaciones>();
            var ds = new DataSet();
            string consulta = "select Materia,Parcial1,Parcial2,Parcial3,Parcial4,ID from v_calificaciones where Alumno = '" + filtro+"'";
            ds = conexion.ObtenerDatos(consulta, "v_calificaciones");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var vista = new VistaCalificaciones
                {
                    //Alumno= row["Alumno"].ToString(),
                    Materia = row["Materia"].ToString(),
                    Parcial1 = Convert.ToInt32(row["Parcial1"]),
                    Parcial2 = Convert.ToInt32(row["Parcial2"]),
                    Parcial3 = Convert.ToInt32(row["Parcial3"]),
                    Parcial4 = Convert.ToInt32(row["Parcial4"]),
                    Id = Convert.ToInt32(row["ID"])
                };

                listVistas.Add(vista);
            }

            return listVistas;
        }

        public DataTable tarervista(string filtro)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select Materia,Parcial1,Parcial2,Parcial3,Parcial4,ID from v_calificaciones where Alumno = '" + filtro + "'";
            ds = conexion.ObtenerDatos(consulta, "v_calificaciones");
            dt = ds.Tables[0];
            return dt;
        }

    }
}
