﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;


namespace AccesoaDatos.ControlEscolar2
{
    public class EscuelaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Escuela escuela)
        {
            if (escuela.Idescuela == 0)
            {
                string consulta = string.Format("Insert into escuela values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                escuela.Nombrescuela, escuela.Rfcescuela, escuela.Domicilioescuela, escuela.Telefono, escuela.Email, escuela.Pagina, escuela.NombreDirector, escuela.LogoEscuela);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update escuela set nombrescuela ='{0}', rfcescuela ='{1}',domicilioescuela='{2}', telefono='{3}',email='{4}',pagina='{5}',nombreDirector='{6}',logoEscuela='{7}' where idescuela={8}",
                 escuela.Nombrescuela,escuela.Rfcescuela,escuela.Domicilioescuela,escuela.Telefono,escuela.Email,escuela.Pagina,escuela.NombreDirector,escuela.LogoEscuela, escuela.Idescuela);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idmateria)
        {
            string consulta = string.Format("Delete from escuela where idescuela={0}", idmateria);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Escuela> GetEscuelas(string filtro)
        {

            var listEscuela = new List<Escuela>();
            var ds = new DataSet();
            string consulta = "Select * from escuela where nombrescuela like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "escuela");

            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var escuela = new Escuela
                {
                    Idescuela = Convert.ToInt32(row["idescuela"]),
                    Nombrescuela = row["nombrescuela"].ToString(),
                    Rfcescuela = row["rfcescuela"].ToString(),
                    Domicilioescuela = row["domicilioescuela"].ToString(),
                    Telefono = row["telefono"].ToString(),
                    Email = row["email"].ToString(),
                    Pagina = row["pagina"].ToString(),
                    NombreDirector = row["nombreDirector"].ToString(),
                    LogoEscuela = row["logoEscuela"].ToString(),
                    
                };

                listEscuela.Add(escuela);
            }

            return listEscuela;
        }

        public DataSet traerDatos()
        {

            var ds = new DataSet();

            //var num = new DataSet("select max(substring(numeroControlMaestro,6)) as NextIncremento from profesores where numeroControlMaestro like '%" + filtro + "%'");
            string consulta = "select * from escuela";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            return ds;

        }

    }
}
