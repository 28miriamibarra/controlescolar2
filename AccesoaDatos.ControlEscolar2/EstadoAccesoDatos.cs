﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class EstadoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EstadoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<Estados> GetEstadosList(ComboBox cm)

        {
            var listEstados = new List<Estados>();
            var ds = new DataSet();
            string consulta = "select codigo from estados";
            ds = conexion.ObtenerDatos(consulta, "estados");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "codigo";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {
                    Fkcodigoestado = row["codigo"].ToString()
                };
                listEstados.Add(estados);
            }
            return listEstados;
        }

        public List<Estados> CodigoEstado (string filtro)
        {
            var listEstado = new List<Estados>();
            var ds = new DataSet();
            string consulta ="Select codigo from estados where nombre  like '%"+ filtro +"%'";
            ds = conexion.ObtenerDatos(consulta, "estados");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {
                    Fkcodigoestado = row["codigo"].ToString()
                };

                listEstado.Add(estados);
            }

            return listEstado;
        }

    }
}
