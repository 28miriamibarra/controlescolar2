﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class VistaAlumnosAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public VistaAlumnosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }


        public List<VistaAlumnosMaterias> GetVistas()
        {

            var listVistas = new List<VistaAlumnosMaterias>();
            var ds = new DataSet();
            string consulta = "select*from v_AlumnosGrupos";
            ds = conexion.ObtenerDatos(consulta, "v_AlumnosGrupos");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var vista = new VistaAlumnosMaterias
                {
                    ID = Convert.ToInt32(row["ID"]),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoPaterno = row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    NumeroControl = row["NumeroControl"].ToString(),
                    Grupo = row["Grupo"].ToString()

                };

                listVistas.Add(vista);
            }

            return listVistas;
        }

    }
}
