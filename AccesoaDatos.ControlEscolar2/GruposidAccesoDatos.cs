﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class GruposidAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public GruposidAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }


        public List<Gruposid> GetGruposID(string filtro)

        {
            var listProfesores = new List<Gruposid>();
            var ds = new DataSet();
            string consulta = "Select idgrupo from grupos where nombregrupo  like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "grupos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Gruposid
                {
                    IdGrupo = Convert.ToInt32(row["idgrupo"].ToString()),

                };

                listProfesores.Add(profesores);
            }

            return listProfesores;
        }

        public List<Gruposid> GetIDList(string nombre, ComboBox cm)
        {
            var listCiudades = new List<Gruposid>();
            var ds = new DataSet();
            string consulta = "Select idgrupo from grupos where nombregrupo = '" + nombre + "'";
            ds = conexion.ObtenerDatos(consulta, "grupos");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "idgrupo";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Gruposid
                {
                    IdGrupo = Convert.ToInt32(row["idgrupo"].ToString())

                };
                listCiudades.Add(ciudades);
            }
            return listCiudades;

        }

        public List<Gruposid> GetNombreGrupos(ComboBox cm)
        {
            var listGrupos = new List<Gruposid>();
            var ds = new DataSet();
            string consulta = "Select nombregrupo from grupos";
            ds = conexion.ObtenerDatos(consulta, "grupos");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombregrupo";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Gruposid
                {
                    Nombregrupo = row["nombregrupo"].ToString()
                };

                listGrupos.Add(grupo);
            }
            return listGrupos;
        }

        public void Guardar(Gruposid grupo)
        {
            if (grupo.IdGrupo == 0)
            {
                string consulta = string.Format("Insert into grupos values(null,'{0}','{1}')",
                grupo.Nombregrupo,grupo.Nomb);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update grupos set nombregrupo ='{0}', nomb ='{1}' where idgrupo={2}",
                 grupo.Nombregrupo, grupo.Nomb, grupo.IdGrupo);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idGrupo)
        {
            string consulta = string.Format("Delete from grupos where idgrupo={0}", idGrupo);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Gruposid> GetGrupos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listEstudios = new List<Gruposid>();
            var ds = new DataSet();
            string consulta = "Select * from grupos where nomb like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "grupos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estudios = new Gruposid
                {
                    IdGrupo= Convert.ToInt32(row["idgrupo"]),
                    Nombregrupo = row["nombregrupo"].ToString(),
                    Nomb = row["nomb"].ToString(),                 
                };

                listEstudios.Add(estudios);
            }


            return listEstudios;
        }

        
    }
}
