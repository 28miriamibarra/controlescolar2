﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;


namespace AccesoaDatos.ControlEscolar2
{
    public class AsignarMateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public AsignarMateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(AsignarMateria materias)
        {

            if (materias.Idasigmateria == 0)
            {
                string consulta = string.Format("insert into asignarmateria values(null,'{0}','{1}','{2}','{3}')",
                  materias.Profesor,materias.Materia,materias.Grupo,materias.Semestre);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update asignarmateria set profesor='{0}', materia ='{1}', grupo='{2}',semestre='{3}'  where idasigmateria={4}",
                 materias.Profesor, materias.Materia, materias.Grupo,materias.Semestre, materias.Idasigmateria);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(string id)
        {
            string consulta = string.Format("Delete from asignarmateria where idasigmateria={0}", id);
            conexion.EjecutarConsulta(consulta);
        }

        public List<AsignarMateria> GetAsignars(string filtro)
        {

            var listMaterias = new List<AsignarMateria>();
            var ds = new DataSet();
            string consulta = "Select * from asignarmateria where materia like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "asignarmateria");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var materias = new AsignarMateria
                {
                    Idasigmateria = Convert.ToInt32(row["idasigmateria"].ToString()),
                    Profesor = row["profesor"].ToString(),
                    Materia = row["materia"].ToString(),
                    Grupo = row["grupo"].ToString(),
                    Semestre = row["semestre"].ToString()
                };

                listMaterias.Add(materias);
            }
            return listMaterias;
        }

        public List<AsignarMateria> GetMateriasnom(ComboBox cm)

        {
            var listProfesores = new List<AsignarMateria>();
            var ds = new DataSet();
            string consulta = "select materia from asignarmateria";
            ds = conexion.ObtenerDatos(consulta, "asignarmateria");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "materia";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new AsignarMateria
                {
                    Materia = row["materia"].ToString()
                };
                listProfesores.Add(profesores);
            }
            return listProfesores;
        }



    }
}
