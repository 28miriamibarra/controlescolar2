﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;


namespace AccesoaDatos.ControlEscolar2
{
    public class EstudiosAccesoDatos
    {

        ConexionAccesoDatos conexion;

        public EstudiosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Estudios estudios)
        {
            if (estudios.Idestudio == 0)
            {
                string consulta = string.Format("Insert into estudios values(null,'{0}','{1}','{2}','{3}')",
                estudios.Nombreestudio,estudios.Anoestudio,estudios.Documento,estudios.Fkprofesor);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update estudios set nombreestudio ='{0}', anoestudio ='{1}',documento='{2}', fkprofesor='{3}' where idestudio={4}",
                 estudios.Nombreestudio, estudios.Anoestudio, estudios.Documento, estudios.Fkprofesor, estudios.Idestudio);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idEstudio)
        {
            string consulta = string.Format("Delete from estudios where idestudio={0}", idEstudio);
            conexion.EjecutarConsulta(consulta);
        }
        /*
        public List<Estudios>GetEstudios(string filtro)
        {

            var listEstudios = new List<Estudios>();
            var ds = new DataSet();
            string consulta = "Select * from estudios where fkprofesor like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var estudios = new Estudios
                {
                    Idestudio = Convert.ToInt32(row["idestudio"]),
                    Nombreestudio = row["nombreestudio"].ToString(),
                    Añoestudio = Convert.ToInt32(row["añoestudio"]),
                    Documento = row["documento"].ToString(),
                    Fkprofesor = Convert.ToInt32(row["fkprofesor"])
                    

                };

                listEstudios.Add(estudios);
            }

            return listEstudios;
        }*/

        public List<Estudios> GetEstudios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listEstudios = new List<Estudios>();
            var ds = new DataSet();
            string consulta = "Select * from estudios where fkprofesor like '%" + filtro + "%'";
            
            ds = conexion.ObtenerDatos(consulta, "estudios");
          

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var estudios = new Estudios
                {

                    Idestudio = Convert.ToInt32(row["idestudio"]),
                    Nombreestudio = row["nombreestudio"].ToString(),
                    Anoestudio = Convert.ToInt32(row["anoestudio"]),
                    Documento = row["documento"].ToString(),
                    Fkprofesor = int.Parse(row["fkprofesor"].ToString())
                };

                listEstudios.Add(estudios);
            }


            return listEstudios;
        }
    }
}
