﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class GruposAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public GruposAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Grupos grupo, Alumno alumno, Gruposid gruposid)
        {
            if (grupo.Idasignar == 0)
            {
                string consulta = string.Format("Insert into asignar values(null,'{0}','{1}')",
                grupo.Fkidalumno, grupo.Fkidgrupo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update asignar set fkidalumno ='{0}', fkidgrupo ='{1}' where idasignar={2}",
                grupo.Fkidalumno, gruposid.IdGrupo, grupo.Idasignar);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idasignar)
        {
            string consulta = string.Format("Delete from asignar where idasignar={0}", idasignar);
            conexion.EjecutarConsulta(consulta);
        }

        public void mostrarVista()
        {
            string consulta = string.Format("select*from v_AlumnosGrupos");
            conexion.EjecutarConsulta(consulta);
        }

        public List<Alumno> GetAlumnoID(string nombre, ComboBox cm)
        {
            var listCiudades = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "Select idalumno from alumno where nombre = '" + nombre + "'";
            ds = conexion.ObtenerDatos(consulta, "alumno");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "idalumno";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Alumno
                {
                    Idalumno = Convert.ToInt32(row["idalumno"].ToString())

                };
                listCiudades.Add(ciudades);
            }
            return listCiudades;

        }

        

        public List<Grupos> traerExistentes(string id, ComboBox cm)
        {
            var listCiudades = new List<Grupos>();
            var ds = new DataSet();
            string consulta = "select fkidalumno from asignar where fkidalumno = '" + id + "'";
            ds = conexion.ObtenerDatos(consulta, "asignar");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "fkidalumno";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Grupos
                {
                    Fkidalumno = Convert.ToInt32(row["fkidalumno"].ToString())

                };
                listCiudades.Add(ciudades);
            }
            return listCiudades;

        }


        public List<Alumno> GetAlumnos(string filtro)
        {

            var listAlumno = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "Select * from alumno where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "alumno");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumno
                {
                    Idalumno = Convert.ToInt32(row["idalumno"]),
                    Numerocontrol = Convert.ToInt32(row["numerocontrol"]),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Fechanac = DateTime.Parse(row["fechanac"].ToString()),
                    Domicilio = row["domicilio"].ToString(),
                    Email = row["email"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Estado = row["fkcodigoestado"].ToString(),
                    Ciudad = row["fkciudad"].ToString()

                };

                listAlumno.Add(alumno);
            }

            return listAlumno;
        }

     

        


       



    }
}
