﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class frmGrupos : Form
    {
        GruposManejador _gruposManejador = new GruposManejador();
        Grupos _grupos = new Grupos();
        VistaAlumnosManejador _vistaAlumnosManejador = new VistaAlumnosManejador();
        GruposidManejador _gruposidManejador = new GruposidManejador();
        Alumno _alumno;
        Gruposid _gruposid;

        public frmGrupos()
        {
            InitializeComponent();
            BuscarVista();
           
            _alumno = new Alumno();
            _gruposid = new Gruposid();
            _gruposidManejador.GetNombreGrupos(cmbGrupos);
            grpEleg.Visible = false;
        }

        private void FrmGrupos_Load(object sender, EventArgs e)
        {
            BuscarAlumnos("");
            btnAlumnos.Enabled = false;
            btnCancelar.Enabled = false;
            btnGuardar.Enabled = false;
           
        }

        private void BuscarVista()
        {
            dtgVista.DataSource = _vistaAlumnosManejador.GetVistas();
        }

        private void CmbGrupos_SelectedIndexChanged(object sender, EventArgs e)
        {
            GruposidManejador _grupposManejador = new GruposidManejador();
            _grupposManejador.GetIDList(cmbGrupos.Text,cmbId).ToString();
            
        }


        private void CargarAlumno()
        {
            _alumno.Idalumno = Convert.ToInt32(cmbIDaL.Text);
            
           
           
           

        }

        private void BuscarAlumnos(string filtro)
        {
            dtgAlumnos.DataSource = _gruposManejador.GetAlumnos(filtro);
        }

        

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumnos(txtBuscar.Text);
        }

        private void ModificarAlumno()
        {
           

            lblId.Text = dtgAlumnos.CurrentRow.Cells["idalumno"].Value.ToString();
            cmbIDaL.Text = dtgAlumnos.CurrentRow.Cells["idalumno"].Value.ToString();
            /* txtNombre.Text = dtgAlumnos.CurrentRow.Cells["nombre"].Value.ToString();
             txtAppat.Text = dtgAlumnos.CurrentRow.Cells["apellidopaterno"].Value.ToString();
             txtApeMat.Text = dtgAlumnos.CurrentRow.Cells["apellidomaterno"].Value.ToString();
             txtDomicilio.Text = dtgAlumnos.CurrentRow.Cells["domicilio"].Value.ToString();
             txtEmail.Text = dtgAlumnos.CurrentRow.Cells["email"].Value.ToString();
             txtNcontrol.Text = dtgAlumnos.CurrentRow.Cells["numerocontrol"].Value.ToString();
             dtpFechan.Text = dtgAlumnos.CurrentRow.Cells["fechanac"].Value.ToString();
             cmbSexo.Text = dtgAlumnos.CurrentRow.Cells["sexo"].Value.ToString();*/
            // cmbEstado.Text = dtgAlumnos.CurrentRow.Cells["fkcodigoestado"].Value.ToString();
            // cmbCiudad.Text = dtgAlumnos.CurrentRow.Cells["fkciudad"].Value.ToString();
            txtNoControl.Text =  dtgAlumnos.CurrentRow.Cells["numerocontrol"].Value.ToString();
            txtNombreA.Text= dtgAlumnos.CurrentRow.Cells["nombre"].Value.ToString();
            _gruposManejador.GetIdAlumnos(txtNombreA.Text, cmbIDaL).ToString();

        }

        private void ModificarAsignacion()
        {
            lblidAsignar.Text = dtgVista.CurrentRow.Cells["ID"].Value.ToString();
           // lblidasig.Text = dtgVista.CurrentRow.Cells["fkidalumno"].Value.ToString();
            cmbGrupos.Text = dtgVista.CurrentRow.Cells["Grupo"].Value.ToString();
            txtNombreA.Text = dtgVista.CurrentRow.Cells["Nombre"].Value.ToString();
            txtNoControl.Text = dtgVista.CurrentRow.Cells["NumeroControl"].Value.ToString();
            
        }

        public void Verificar()
        {
            _gruposManejador.traerExistentes(comboBox1.Text, cmbIDaL).ToString();
        }

        private void DtgAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarAlumno();
            _gruposManejador.traerExistentes(lblId.Text, comboBox1).ToString();

            if (comboBox1.Items.Count == 0)
            {


                BuscarAlumnos("");
                grpEleg.Visible = false;
                grpVista.Visible = true;
            }
            else
            {
                MessageBox.Show("El alumno ya existe en un grupo", "Error");

            }


        }

        private void GuardarAsig()
        {
            _gruposManejador.Guardar(_grupos, _alumno, _gruposid);

        }
        public void CargarAsig()
        {
            _grupos.Idasignar = Convert.ToInt32(lblidAsignar.Text);
            _alumno.Idalumno = Convert.ToInt32(lblId.Text);
            _grupos.Fkidalumno= Convert.ToInt32(cmbIDaL.Text);
            _gruposid.IdGrupo = Convert.ToInt32(cmbId.Text);
            _grupos.Fkidgrupo = Convert.ToInt32(cmbId.Text);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarAlumno();
            CargarAsig();
         
                if (true)
                {
                    try
                    {
                        GuardarAsig();
                        BuscarAlumnos("");
                        BuscarVista();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                } 

            btnGuardar.Enabled = false;
            btnEliminar.Enabled = true;
            btnCancelar.Enabled = false;
            btnAlumnos.Enabled = true;
        }

        private void EliminarAsignacion()
        {
            var idAsig = dtgVista.CurrentRow.Cells["ID"].Value;
            _gruposManejador.Eliminar(Convert.ToInt32(idAsig));
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            txtNombreA.Text = "";
            txtNoControl.Text = "";
            cmbIDaL.Text = "";
            cmbIDaL.Text = "";
        }

        private void GrpEleg_Enter(object sender, EventArgs e)
        {

        }

        private void DtgVista_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarAsignacion();
                    BuscarAlumnos("");
                    BuscarVista();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void DtgVista_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarAsignacion();
            
            txtNoControl.Enabled = false;
            txtNombreA.Enabled = false;

             
            _gruposManejador.GetIdAlumnos(txtNombreA.Text, cmbIDaL).ToString();
            btnGuardar.Enabled = true;
        }

        private void GrpVista_Enter(object sender, EventArgs e)
        {

        }

        private void BtnAlumnos_Click(object sender, EventArgs e)
        {
            grpVista.Visible = false;
            grpEleg.Visible = true;
        }

        private void BtnCanceLARaL_Click(object sender, EventArgs e)
        {
            grpVista.Visible = true;
            grpEleg.Visible = false;


        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            btnAlumnos.Enabled = true;
            btnCancelar.Enabled = true;
            btnGuardar.Enabled = true;
            btnEliminar.Enabled = false;
        }

        private void DtgAlumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
