﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using System.IO;





namespace ControlEscolar2
{
    public partial class frmEscuela : Form
    {
        private OpenFileDialog _imagenJpg; //Clase permite abrir un dialogo para elegir ubicacion en la unidad c:
        private OpenFileDialog _logoEscuela;
        private string _ruta;
        private EscuelaManejador _escuelaManejador;
        private Escuela _escuela;
        

        public frmEscuela()
        {
            InitializeComponent();
            _escuelaManejador = new EscuelaManejador();
            _escuela = new Escuela();
            _imagenJpg = new OpenFileDialog();
            _logoEscuela = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Logo\\";
            
        }
        private void CargarEscuela()
        {

           
            
                _escuela.Idescuela = Convert.ToInt32(lblId.Text);
                _escuela.Nombrescuela = txtNombre.Text;
                _escuela.Rfcescuela = txtRfc.Text;
                _escuela.Domicilioescuela = txtDomicilio.Text;
                _escuela.Telefono = txtTel.Text;
                _escuela.Email = txtEmail.Text;
                _escuela.Pagina = txtPagina.Text;
                _escuela.NombreDirector = txtdirec.Text;
                _escuela.LogoEscuela = txtLogo.Text;
            
        }

        public void traerDatos()
        {
            var ds = new DataSet();
            ds = _escuelaManejador.traerDatos();

            lblId.Text = ds.Tables[0].Rows[0]["idescuela"].ToString();
            txtNombre.Text = ds.Tables[0].Rows[0]["nombrescuela"].ToString();
            txtRfc.Text= ds.Tables[0].Rows[0]["rfcescuela"].ToString();
            txtDomicilio.Text= ds.Tables[0].Rows[0]["domicilioescuela"].ToString();
            txtTel.Text = ds.Tables[0].Rows[0]["telefono"].ToString();
            txtEmail.Text = ds.Tables[0].Rows[0]["email"].ToString();
            txtPagina.Text= ds.Tables[0].Rows[0]["pagina"].ToString();
            txtdirec.Text = ds.Tables[0].Rows[0]["nombreDirector"].ToString();
            txtLogo.Text= ds.Tables[0].Rows[0]["logoEscuela"].ToString();

        }

        private void ControlarBotones( bool Guardar, bool Cancelar, bool Eliminar)
        {
            
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            //btnEliminar.Enabled = Eliminar;

        }

        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtdirec.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtEmail.Enabled = activar;
            txtLogo.Enabled = activar;
            txtPagina.Enabled = activar;
            txtRfc.Enabled = activar;
            txtTel.Enabled = activar;
            
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtdirec.Text = "";
            txtDomicilio.Text = "";
            txtEmail.Text = "";
            txtLogo.Text = "";
            txtPagina.Text = "";
            txtRfc.Text = "";
            txtTel.Text = "";
            lblId.Text = "0";
            imgLogo.Image = null;
            
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            //BuscarEscuela(txtBuscar.Text);
        }

        private void FrmEscuela_Load(object sender, EventArgs e)
        {
            imgLogo.Enabled = false;
            btnCargar.Enabled = false;
            btnEliminae.Enabled = false;
            traerDatos();
            //BuscarEscuela("");
            MostrarImagen();
            ControlarCuadros(false);

        }

        private void BuscarEscuela(string filtro)
        {
            //dgvEscuela.DataSource = _escuelaManejador.GetEscuelas(filtro);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarEscuela();
            if (true)
            {
                try
                {
                    GuardarEscuela();
                    LimpiarCuadros();
                    BuscarEscuela("");
                    ControlarBotones(false, false, true);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void GuardarEscuela()
        {
            _escuelaManejador.Guardar(_escuela);

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            
        }

        /*private void EliminarEscuela()
        {
            var ds = new DataSet();
            ds = _escuelaManejador.traerDatos();

            var Idescuela = ds.Tables[0].Rows[0]["Idescuela"].ToString();
            _escuelaManejador.Eliminar(Convert.ToInt32(Idescuela));
        }*/
        /*private void ModificarEscuela()
        {
            ControlarCuadros(true);
            ControlarBotones(true, true, false);

            

            lblId.Text = dgvEscuela.CurrentRow.Cells["idescuela"].Value.ToString();
            txtNombre.Text = dgvEscuela.CurrentRow.Cells["nombrescuela"].Value.ToString();
            txtRfc.Text = dgvEscuela.CurrentRow.Cells["rfcescuela"].Value.ToString();
            txtDomicilio.Text = dgvEscuela.CurrentRow.Cells["domicilioescuela"].Value.ToString();
            txtTel.Text = dgvEscuela.CurrentRow.Cells["telefono"].Value.ToString();
            txtEmail.Text = dgvEscuela.CurrentRow.Cells["email"].Value.ToString();
            txtPagina.Text = dgvEscuela.CurrentRow.Cells["pagina"].Value.ToString();
            txtdirec.Text = dgvEscuela.CurrentRow.Cells["nombreDirector"].Value.ToString();
            txtLogo.Text = dgvEscuela.CurrentRow.Cells["logoEscuela"].Value.ToString();
            //var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg|*.png");
            //imgLogo.ImageLocation = txtLogo.Text;
            //imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;
            MostrarImagen();
        }*/

        private void MostrarImagen()
        {
            //OpenFileDialog BuscarImagen = new OpenFileDialog();

            /*this.imgLogo.Text = BuscarImagen.FileName;
            String Direccion = txtLogo.Text;
            this.imgLogo.ImageLocation = Direccion;*/
            //string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            //textBox1.Text = _ruta;
            //appPath.ToString();
            this.imgLogo.ImageLocation = _ruta + txtLogo.Text;
            imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;

        }

        private void DgvEscuela_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //ModificarEscuela();
            BuscarEscuela("");
        }

        private void BtnGuardar_Click_1(object sender, EventArgs e)
        {
            CargarEscuela();
            if (true)
            {
                if(txtLogo.Visible== false)
                {
                    MessageBox.Show("Es necesario insertar un logotipo");
                }
                else
                {
                    try
                    {
                        GuardarEscuela();
                        //LimpiarCuadros();
                        BuscarEscuela("");
                        ControlarBotones(false, false, true);
                        ControlarCuadros(false);
                        GuardarImagenJpg();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
                
            }
        }

        private void BtnEliminar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                   // EliminarEscuela();
                    BuscarEscuela("");
                    EliminarImagenJpg();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        //Carga Archivos:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

        private void CargarImagenJpg()
        {
            //_imagenJpg.Filter = "Imagen tipo (*.jpg)|*.jpg|Imagen tipo (*.png)|*.png";
            _imagenJpg.Filter = "Imagen tipo (*.jpg)|*.jpg";
            _imagenJpg.Title = "Cargar Imagen";
            _imagenJpg.ShowDialog();

            if (_imagenJpg.FileName != "")
            {
                var archivo = new FileInfo(_imagenJpg.FileName);
                if (archivo.Length > 1000000)
                {
                    MessageBox.Show("imagen demasiado grande para guardar en la base de datos");
                    btnGuardar.Enabled = false;
                }
                else
                {
                    txtLogo.Text = archivo.Name;
                    imgLogo.ImageLocation = _imagenJpg.FileName;
                    imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void BtnCargar_Click(object sender, EventArgs e)
        {
            CargarImagenJpg();
            var archivo = new FileInfo(_imagenJpg.FileName);
            txtLogo.Text = archivo.Name;
        }

        private void GuardarImagenJpg()
        {
            if (_imagenJpg.FileName != null)
            {
                if (_imagenJpg.FileName != "")
                {
                    var archivo = new FileInfo(_imagenJpg.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        //Codigo para agregar el archivo
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");

                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para reemplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }

                    }
                    else
                    {
                        //crea ruta
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }
            }
        }

        private void EliminarImagenJpg()
        {
            if (MessageBox.Show("Estas seguro de eliminar el logotipo", "Eliminar Logotipo",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {
                    //Codigo para agregar el archivo
                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");

                    FileInfo archivoAnterior;

                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para reemplazar imagen
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            
                            //txtLogo.Text = "";
                            archivoAnterior.Delete();
                            
                        }
                    }
                }
            }
        }

        private void BtnEliminae_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de eliminar el logotipo", "Eliminar Logotipo",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //EliminarImagenJpg();

                txtLogo.Visible = false;
                imgLogo.Image = null;
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
           // imgLogo.Image = null;
            //txtLogo.Text = "";

            ControlarBotones( false, false, true);
            ControlarCuadros(false);
            this.imgLogo.ImageLocation = _ruta + txtLogo.Text;
            imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;

            var ds = new DataSet();
            ds = _escuelaManejador.traerDatos();
            txtLogo.Text = ds.Tables[0].Rows[0]["logoEscuela"].ToString();
            txtLogo.Visible = true;

            //string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            //this.imgLogo.ImageLocation = appPath + txtLogo.Text;
            //textBox1.Text = _ruta;
            // appPath.ToString();
            //EliminarImagenJpg();
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            imgLogo.Enabled = true;
            btnEliminae.Enabled = true;
            btnCargar.Enabled = true;
            ControlarCuadros(true);
            ControlarBotones(true, true, true);
        }

        private void ImgLogo_Click(object sender, EventArgs e)
        {

        }
    }
}
