﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using Microsoft.Office.Interop.Excel;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing.Printing;

namespace ControlEscolar2
{
    public partial class frmCalificaciones : Form
    {
        AlumnoManejador _alumnomanejador = new AlumnoManejador();
        VistaCalificacionesManejador _vistaManejador = new VistaCalificacionesManejador();
        MateriasManejador _materiasManejador = new MateriasManejador();
        Calificaciones calificaciones = new Calificaciones();
        CalificacionesManejador _calificacionesManejador = new CalificacionesManejador();
        AsignarMateriaManejador _asignarMateriaManejador = new AsignarMateriaManejador();
        public string _ruta;
        string ruta = Directory.GetCurrentDirectory();
        


        bool terminado = true;
        public frmCalificaciones()
        {
            InitializeComponent();
            _alumnomanejador.GetNombreAlumno(cmbAlumno);
            _alumnomanejador.GetNombreAlumno(cmbAUM);
            _asignarMateriaManejador.GetMateriasnom(cmbMaterias);
            ControlarBotones(true, false, false);
            _ruta = System.Windows.Forms.Application.StartupPath + "\\Boletas\\";
            ruta = ruta + "\\inscriptos.txt";
            
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;


        }

        private void ControlarCuadros(bool activar)
        {
            cmbidmat.Enabled = activar;
            cmbMaterias.Enabled = activar;
            cmbAUM.Enabled = activar;
            cmbidal.Enabled = activar;
            txtParcial1.Enabled = activar;
            txtParcial2.Enabled = activar;
            txtParcial3.Enabled = activar;
            txtParcial4.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            txtParcial1.Text = "";
            txtParcial2.Text = "";
            txtParcial3.Text = "";
            txtParcial4.Text = "";
            lblId.Text = "0";
            cmbMaterias.Text = "";
            cmbAUM.Text = "";

        }

        private void FrmCalificaciones_Load(object sender, EventArgs e)
        {
            ControlarCuadros(false);
            cmbMaterias.Text = "";
            cmbAUM.Text = "";
            btnExportar.Enabled = false;
            btnPdf.Enabled = false;

        }

        private void CmbAlumno_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarVista();
            CalificacionesManejador _calificacionesManejador = new CalificacionesManejador();
            _calificacionesManejador.GetIdAlumno(cmbAlumno.Text, cmbidal).ToString();
        }
        private void BuscarVista()
        {
            dtgVista.DataSource = _vistaManejador.GetVistas(cmbAlumno.Text);
            btnExportar.Enabled = true;
            btnPdf.Enabled = true;
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcel(dtgVista);
        }

        public bool ExportarDataGridViewExcel(DataGridView grd)
        {

            try
            {
                try
                {
                    SaveFileDialog fichero = new SaveFileDialog();
                    fichero.Filter = "Excel (*.xls)|*.xls";
                    if (fichero.ShowDialog() == DialogResult.OK)
                    {
                        Microsoft.Office.Interop.Excel.Application aplicacion;
                        Workbook libros_trabajo;
                        Worksheet hoja_trabajo;
                        aplicacion = new Microsoft.Office.Interop.Excel.Application();
                        libros_trabajo = aplicacion.Workbooks.Add();
                        hoja_trabajo =
                          (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                        //Recorremos el dataGridView rellenando la hoja

                        for (int i = 0; i < grd.Rows.Count; i++)
                        {
                            for (int j = 0; j < grd.Columns.Count; j++)
                            {
                                hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();

                            }
                        }

                        libros_trabajo.SaveAs(fichero.FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                        libros_trabajo.Close(true);
                        aplicacion.Quit();

                        MessageBox.Show("Reporte Terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Fallo la creacion del archivo intenta de nuevo con otro nombre", "Error de creacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        private void BgwExcel_DoWork(object sender, DoWorkEventArgs e)
        {

            for (int i = 0; i <= 100; i++)
            {
                terminado = ExportarDataGridViewExcel(dtgVista);
                bgwExcel.ReportProgress(i);
            }

        }

        private void BgwExcel_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgbExcel.Value = e.ProgressPercentage;
            lblPorc.Text = e.ProgressPercentage.ToString() + "  %";
        }

        private void BgwExcel_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (terminado)
            {
                MessageBox.Show("El proceso se completo correctamente");

            }
            else
            {
                MessageBox.Show("El proceso fallo");
            }
        }

        public void ExportarTabla_PDF(System.Data.DataTable dtblTable, String strPdfPath, string strHeader, string alumno)
        {
            
        }

    


        private void BtnPdf_Click(object sender, EventArgs e)
         {
            PrintDocument doc = new PrintDocument();
            doc.DefaultPageSettings.Landscape = true;
            doc.PrinterSettings.PrinterName = "Microsoft Print to PDF";

            PrintPreviewDialog ppd = new PrintPreviewDialog { Document = doc };
            ((Form)ppd).WindowState = FormWindowState.Maximized;

            doc.PrintPage += delegate (object ev, PrintPageEventArgs ep)
            {
                const int DGV_ALTO = 35;
                int left = ep.MarginBounds.Left, top = ep.MarginBounds.Top;

                foreach (DataGridViewColumn col in dtgVista.Columns)
                {
                    ep.Graphics.DrawString(col.HeaderText, new System.Drawing.Font("Segoe UI", 16, FontStyle.Bold), Brushes.Aqua, left, top);
                    left += col.Width;

                    if (col.Index < dtgVista.ColumnCount - 1)
                        ep.Graphics.DrawLine(Pens.Gray, left - 5, top, left - 5, top + 43 + (dtgVista.RowCount - 1) * DGV_ALTO);
                }
                left = ep.MarginBounds.Left;
                ep.Graphics.FillRectangle(Brushes.Black, left, top + 40, ep.MarginBounds.Right - left, 3);
                top += 43;

                foreach (DataGridViewRow row in dtgVista.Rows)
                {
                    if (row.Index == dtgVista.RowCount - 1) break;
                    left = ep.MarginBounds.Left;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        ep.Graphics.DrawString(Convert.ToString(cell.Value), new System.Drawing.Font("Segoe UI", 13), Brushes.Black, left, top + 4);
                        left += cell.OwningColumn.Width;
                    }
                    top += DGV_ALTO;
                    ep.Graphics.DrawLine(Pens.Gray, ep.MarginBounds.Left, top, ep.MarginBounds.Right, top);
                }
            };
            ppd.ShowDialog();
            
        }




        //************************************************Calificaciones************************************************************


        private void CargarParciales()
        {
            calificaciones.Idcal = Convert.ToInt32(lblId.Text);
            calificaciones.Parcial1 = Convert.ToInt32(txtParcial1.Text);
            calificaciones.Parcial2 = Convert.ToInt32(txtParcial2.Text);
            calificaciones.Parcial3 = Convert.ToInt32(txtParcial3.Text);
            calificaciones.Parcial4 = Convert.ToInt32(txtParcial4.Text);
            calificaciones.Fkmateria = Convert.ToInt32(cmbidmat.Text);
            calificaciones.Fkalumno= Convert.ToInt32(cmbidal.Text);
        }
        



        private void CmbAUM_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalificacionesManejador _calificacionesManejador = new CalificacionesManejador();
            _calificacionesManejador.GetIdAlumno(cmbAUM.Text, cmbidal).ToString();
            cmbMaterias.Enabled = true;
        }

        public void cargarId()
        {
            CalificacionesManejador _calificacionesManejador = new CalificacionesManejador();
            _calificacionesManejador.GetIDList(cmbMaterias.Text, cmbidmat).ToString();
        }

        private void CmbMaterias_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarId();
        }

        private void GuardarParciales()
        {
            _calificacionesManejador.Guardar(calificaciones);

        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarParciales();
            if (true)
            {
                try
                {
                    GuardarParciales();
                    LimpiarCuadros();
                    BuscarVista();
                    ControlarBotones(true, false, false);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ModificarCalif()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true);

            lblId.Text = dtgVista.CurrentRow.Cells["ID"].Value.ToString();
            txtParcial1.Text = dtgVista.CurrentRow.Cells["Parcial1"].Value.ToString();
            txtParcial2.Text = dtgVista.CurrentRow.Cells["Parcial2"].Value.ToString();
            txtParcial3.Text = dtgVista.CurrentRow.Cells["Parcial3"].Value.ToString();
            txtParcial4.Text = dtgVista.CurrentRow.Cells["Parcial4"].Value.ToString();
            cmbAUM.Text = cmbAlumno.Text;
            cmbMaterias.Text= dtgVista.CurrentRow.Cells["Materia"].Value.ToString();
        }

        private void DtgVista_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {          
            try
            {              
                ModificarCalif();
                cargarId();
                cmbMaterias.Enabled = false;
                cmbAlumno.Enabled = false;
                cmbAUM.Enabled = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void CmbAUM_TextUpdate(object sender, EventArgs e)
        {
            
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarCuadros(true);
            cmbMaterias.Enabled = false;
            ControlarBotones(false, true, true);
            cmbAUM.Text = cmbAlumno.Text;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void DtgVista_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
