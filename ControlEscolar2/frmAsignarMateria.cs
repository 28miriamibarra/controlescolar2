﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;


namespace ControlEscolar2
{
    public partial class frmAsignarMateria : Form
    {
        AsignarMateriaManejador _asignarMateriaManejador = new AsignarMateriaManejador();
        AsignarMateria _asignarMateria = new AsignarMateria();
        MateriasManejador _materiasManejador = new MateriasManejador();
        GruposidManejador _gruposManejador = new GruposidManejador();

        public frmAsignarMateria()
        {
            InitializeComponent();
        }

        private void FrmAsignarMateria_Load(object sender, EventArgs e)
        {
            BuscarMaterias("");
            _asignarMateriaManejador.GetProfesoresLists(cmbProfesor);
            _materiasManejador.GetMateriasnom(cmMateria);
            _gruposManejador.GetNombreGrupos(cmbGrupo);
            ControlarCuadros(false);
        }

        private void CargarMaterias()
        {
            _asignarMateria.Idasigmateria = Convert.ToInt32(lblId.Text);
            _asignarMateria.Materia = cmMateria.Text;
            _asignarMateria.Profesor = cmbProfesor.Text;
            _asignarMateria.Grupo = cmbGrupo.Text;
            _asignarMateria.Semestre = comboBox1.Text;
        }

        private void BuscarMaterias(string filtro)
        {
            dtgMateri.DataSource = _asignarMateriaManejador.GetMaterias(filtro);
        }

        private void GuardarMateria()
        {
            _asignarMateriaManejador.Guardar(_asignarMateria);

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        private void ControlarCuadros(bool activar)
        {
            cmbGrupo.Enabled = activar;
            cmbProfesor.Enabled = activar;
            cmMateria.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            cmMateria.Text = "";
            cmbProfesor.Text = "";
            cmbGrupo.Text = "";
            lblId.Text = "0";

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarMaterias();
            if (true)
            {
                try
                {
                    GuardarMateria();
                    LimpiarCuadros();
                    BuscarMaterias("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaterias(txtBuscar.Text);
        }
        private void EliminarMateria()
        {
            string Idmateria = dtgMateri.CurrentRow.Cells["idasigmateria"].Value.ToString();
            _asignarMateriaManejador.Eliminar(Idmateria);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarMateria();
                    BuscarMaterias("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void ModificarMaterias()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgMateri.CurrentRow.Cells["idasigmateria"].Value.ToString();
            cmbGrupo.Text = dtgMateri.CurrentRow.Cells["grupo"].Value.ToString();
            cmbProfesor.Text = dtgMateri.CurrentRow.Cells["profesor"].Value.ToString();
            cmMateria.Text = dtgMateri.CurrentRow.Cells["materia"].Value.ToString();
            comboBox1.Text = dtgMateri.CurrentRow.Cells["semestre"].Value.ToString();
        }

        private void DtgMateri_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarMaterias();
            BuscarMaterias("");
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

        }

        private void CmbProfesor_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarCuadros(false);
        }
    }
}
