﻿namespace ControlEscolar2
{
    partial class frmCalificaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalificaciones));
            this.dtgVista = new System.Windows.Forms.DataGridView();
            this.cmbAlumno = new System.Windows.Forms.ComboBox();
            this.btnExportar = new System.Windows.Forms.Button();
            this.pgbExcel = new System.Windows.Forms.ProgressBar();
            this.lblPorc = new System.Windows.Forms.Label();
            this.bgwExcel = new System.ComponentModel.BackgroundWorker();
            this.cmbAUM = new System.Windows.Forms.ComboBox();
            this.cmbMaterias = new System.Windows.Forms.ComboBox();
            this.txtParcial1 = new System.Windows.Forms.TextBox();
            this.txtParcial4 = new System.Windows.Forms.TextBox();
            this.txtParcial3 = new System.Windows.Forms.TextBox();
            this.txtParcial2 = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.cmbidal = new System.Windows.Forms.ComboBox();
            this.cmbidmat = new System.Windows.Forms.ComboBox();
            this.lblParc = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPdf = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgVista)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgVista
            // 
            this.dtgVista.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dtgVista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgVista.Location = new System.Drawing.Point(25, 47);
            this.dtgVista.Name = "dtgVista";
            this.dtgVista.Size = new System.Drawing.Size(504, 150);
            this.dtgVista.TabIndex = 0;
            this.dtgVista.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgVista_CellContentClick);
            this.dtgVista.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgVista_CellDoubleClick);
            // 
            // cmbAlumno
            // 
            this.cmbAlumno.FormattingEnabled = true;
            this.cmbAlumno.Location = new System.Drawing.Point(408, 19);
            this.cmbAlumno.Name = "cmbAlumno";
            this.cmbAlumno.Size = new System.Drawing.Size(121, 21);
            this.cmbAlumno.TabIndex = 1;
            this.cmbAlumno.SelectedIndexChanged += new System.EventHandler(this.CmbAlumno_SelectedIndexChanged);
            // 
            // btnExportar
            // 
            this.btnExportar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnExportar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExportar.BackgroundImage")));
            this.btnExportar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnExportar.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnExportar.Location = new System.Drawing.Point(445, 203);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(49, 51);
            this.btnExportar.TabIndex = 2;
            this.btnExportar.UseVisualStyleBackColor = false;
            this.btnExportar.Click += new System.EventHandler(this.BtnExportar_Click);
            // 
            // pgbExcel
            // 
            this.pgbExcel.ForeColor = System.Drawing.Color.Green;
            this.pgbExcel.Location = new System.Drawing.Point(25, 220);
            this.pgbExcel.Name = "pgbExcel";
            this.pgbExcel.Size = new System.Drawing.Size(394, 23);
            this.pgbExcel.TabIndex = 3;
            // 
            // lblPorc
            // 
            this.lblPorc.AutoSize = true;
            this.lblPorc.Location = new System.Drawing.Point(439, 222);
            this.lblPorc.Name = "lblPorc";
            this.lblPorc.Size = new System.Drawing.Size(0, 13);
            this.lblPorc.TabIndex = 4;
            // 
            // bgwExcel
            // 
            this.bgwExcel.WorkerReportsProgress = true;
            this.bgwExcel.WorkerSupportsCancellation = true;
            this.bgwExcel.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BgwExcel_DoWork);
            this.bgwExcel.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BgwExcel_ProgressChanged);
            this.bgwExcel.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BgwExcel_RunWorkerCompleted);
            // 
            // cmbAUM
            // 
            this.cmbAUM.FormattingEnabled = true;
            this.cmbAUM.Location = new System.Drawing.Point(23, 96);
            this.cmbAUM.Name = "cmbAUM";
            this.cmbAUM.Size = new System.Drawing.Size(113, 21);
            this.cmbAUM.TabIndex = 5;
            this.cmbAUM.SelectedIndexChanged += new System.EventHandler(this.CmbAUM_SelectedIndexChanged);
            this.cmbAUM.TextUpdate += new System.EventHandler(this.CmbAUM_TextUpdate);
            // 
            // cmbMaterias
            // 
            this.cmbMaterias.FormattingEnabled = true;
            this.cmbMaterias.Location = new System.Drawing.Point(147, 96);
            this.cmbMaterias.Name = "cmbMaterias";
            this.cmbMaterias.Size = new System.Drawing.Size(101, 21);
            this.cmbMaterias.TabIndex = 6;
            this.cmbMaterias.SelectedIndexChanged += new System.EventHandler(this.CmbMaterias_SelectedIndexChanged);
            // 
            // txtParcial1
            // 
            this.txtParcial1.Location = new System.Drawing.Point(260, 97);
            this.txtParcial1.Name = "txtParcial1";
            this.txtParcial1.Size = new System.Drawing.Size(62, 20);
            this.txtParcial1.TabIndex = 8;
            this.txtParcial1.Text = "0";
            // 
            // txtParcial4
            // 
            this.txtParcial4.Location = new System.Drawing.Point(508, 97);
            this.txtParcial4.Name = "txtParcial4";
            this.txtParcial4.Size = new System.Drawing.Size(63, 20);
            this.txtParcial4.TabIndex = 9;
            this.txtParcial4.Text = "0";
            // 
            // txtParcial3
            // 
            this.txtParcial3.Location = new System.Drawing.Point(425, 97);
            this.txtParcial3.Name = "txtParcial3";
            this.txtParcial3.Size = new System.Drawing.Size(61, 20);
            this.txtParcial3.TabIndex = 10;
            this.txtParcial3.Text = "0";
            // 
            // txtParcial2
            // 
            this.txtParcial2.Location = new System.Drawing.Point(340, 97);
            this.txtParcial2.Name = "txtParcial2";
            this.txtParcial2.Size = new System.Drawing.Size(63, 20);
            this.txtParcial2.TabIndex = 11;
            this.txtParcial2.Text = "0";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(399, 13);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(13, 13);
            this.lblId.TabIndex = 12;
            this.lblId.Text = "0";
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnGuardar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardar.BackgroundImage")));
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardar.Location = new System.Drawing.Point(476, 10);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(46, 48);
            this.btnGuardar.TabIndex = 26;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(528, 10);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(46, 48);
            this.btnCancelar.TabIndex = 27;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNuevo.BackgroundImage")));
            this.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNuevo.Location = new System.Drawing.Point(418, 10);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(52, 48);
            this.btnNuevo.TabIndex = 25;
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // cmbidal
            // 
            this.cmbidal.FormattingEnabled = true;
            this.cmbidal.Location = new System.Drawing.Point(418, 13);
            this.cmbidal.Name = "cmbidal";
            this.cmbidal.Size = new System.Drawing.Size(38, 21);
            this.cmbidal.TabIndex = 29;
            // 
            // cmbidmat
            // 
            this.cmbidmat.FormattingEnabled = true;
            this.cmbidmat.Location = new System.Drawing.Point(476, 13);
            this.cmbidmat.Name = "cmbidmat";
            this.cmbidmat.Size = new System.Drawing.Size(38, 21);
            this.cmbidmat.TabIndex = 30;
            // 
            // lblParc
            // 
            this.lblParc.AutoSize = true;
            this.lblParc.Location = new System.Drawing.Point(22, 31);
            this.lblParc.Name = "lblParc";
            this.lblParc.Size = new System.Drawing.Size(13, 13);
            this.lblParc.TabIndex = 31;
            this.lblParc.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(259, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "PARCIAL 1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(343, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "PARCIAL 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(426, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "PARCIAL 3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(505, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "PARCIAL 4";
            this.label3.Click += new System.EventHandler(this.Label3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPdf);
            this.groupBox1.Controls.Add(this.lblParc);
            this.groupBox1.Controls.Add(this.lblPorc);
            this.groupBox1.Controls.Add(this.pgbExcel);
            this.groupBox1.Controls.Add(this.btnExportar);
            this.groupBox1.Controls.Add(this.cmbAlumno);
            this.groupBox1.Controls.Add(this.dtgVista);
            this.groupBox1.Location = new System.Drawing.Point(23, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(551, 282);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            // 
            // btnPdf
            // 
            this.btnPdf.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnPdf.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPdf.BackgroundImage")));
            this.btnPdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPdf.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnPdf.Location = new System.Drawing.Point(494, 203);
            this.btnPdf.Name = "btnPdf";
            this.btnPdf.Size = new System.Drawing.Size(49, 51);
            this.btnPdf.TabIndex = 32;
            this.btnPdf.UseVisualStyleBackColor = false;
            this.btnPdf.Click += new System.EventHandler(this.BtnPdf_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label5.Location = new System.Drawing.Point(18, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 25);
            this.label5.TabIndex = 87;
            this.label5.Text = "Calificaciones";
            // 
            // frmCalificaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(597, 432);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.txtParcial2);
            this.Controls.Add(this.txtParcial3);
            this.Controls.Add(this.txtParcial4);
            this.Controls.Add(this.txtParcial1);
            this.Controls.Add(this.cmbMaterias);
            this.Controls.Add(this.cmbAUM);
            this.Controls.Add(this.cmbidmat);
            this.Controls.Add(this.cmbidal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCalificaciones";
            this.Text = "frmCalificaciones";
            this.Load += new System.EventHandler(this.FrmCalificaciones_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgVista)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgVista;
        private System.Windows.Forms.ComboBox cmbAlumno;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.ProgressBar pgbExcel;
        private System.Windows.Forms.Label lblPorc;
        private System.ComponentModel.BackgroundWorker bgwExcel;
        private System.Windows.Forms.ComboBox cmbAUM;
        private System.Windows.Forms.ComboBox cmbMaterias;
        private System.Windows.Forms.TextBox txtParcial1;
        private System.Windows.Forms.TextBox txtParcial4;
        private System.Windows.Forms.TextBox txtParcial3;
        private System.Windows.Forms.TextBox txtParcial2;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.ComboBox cmbidal;
        private System.Windows.Forms.ComboBox cmbidmat;
        private System.Windows.Forms.Label lblParc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPdf;
    }
}