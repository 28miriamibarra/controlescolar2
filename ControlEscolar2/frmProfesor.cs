﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

using System.IO;

namespace ControlEscolar2
{
    public partial class frmProfesor : Form   
    {
        private OpenFileDialog _ArchivoEstudios;

        private ProfesorManejador _profesormanejador = new ProfesorManejador();
        private EstudiosManejador _estudiosManejador = new EstudiosManejador();
        private Estudios _estudios;
        private Profesores _profesores;
        private string _ruta;
        //string incre = "1";


        public frmProfesor()
        {           
            InitializeComponent();
            _profesormanejador = new ProfesorManejador();
            _profesores = new Profesores();
            _estudiosManejador = new EstudiosManejador();
            _estudios = new Estudios();
            grpEstudios.Visible = false;
            grpAgregar.Visible = false;
            txtBuscarEst.Text = "";
            _ArchivoEstudios = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Archivos\\";


        }


        private void cargarProfesores()
        {
            _profesores.Llave = Convert.ToInt32(lblId.Text);
           
            _profesores.NumeroControlMaestro = txtNumeroControl.Text;
            _profesores.Nombre = txtNombre.Text;
            _profesores.ApellidoPaterno = txtApellidoPat.Text;
            _profesores.ApellidoMaterno = txtApellidoMat.Text;
            _profesores.Direccion = txtDireccion.Text;
            _profesores.FechaNac = Convert.ToDateTime(dtpFechan.Text);
            _profesores.Ciudad = cmbCiudad.Text;
            _profesores.Estado = cmbEstado.Text;
            _profesores.NumeroCedula = Convert.ToInt32( txtNumeroCedula.Text);
            _profesores.Titulo = txtTitulo.Text;
            _profesores.Nombre = txtMaestrod.Text;
        }


        private void BuscarProfesores(string filtro)
        {
            dtgProf.DataSource = _profesormanejador.GetProfesores(filtro);
        }

        private void GuardarProfesor()
        {
            _profesormanejador.Guardar(_profesores);

        }

        private void EliminarProfesor()
        {
            var NoControl = dtgProf.CurrentRow.Cells["Llave"].Value;
            _profesormanejador.Eliminar(NoControl.ToString());
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }

        private void ControlarCuadros(bool activar)
        {
            txtNumeroControl.Enabled = activar;
            txtNombre.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtApellidoPat.Enabled = activar;
            txtApellidoMat.Enabled = activar;
            txtNumeroCedula.Enabled = activar;
            txtTitulo.Enabled = activar;
            dtpFechan.Enabled = activar;        
            cmbEstado.Enabled = activar;
            cmbCiudad.Enabled = activar;
            dtgañoIngreso.Enabled = activar;
            btnEstudios.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApellidoPat.Text = "";
            txtApellidoMat.Text = "";
            txtDireccion.Text = "";
            txtTitulo.Text = "";
            txtNumeroCedula.Text = "";
            txtNumeroControl.Text = "";
            dtpFechan.Text = "";
            lblId.Text = "0";
            cmbEstado.Text = "";
            cmbCiudad.Text = "";
            dtgañoIngreso.Text = "";
        }

        private void ModificarProfesor()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblId.Text = dtgProf.CurrentRow.Cells["llave"].Value.ToString();
            txtNumeroControl.Text = dtgProf.CurrentRow.Cells["numeroControlMaestro"].Value.ToString();
            txtNombre.Text= dtgProf.CurrentRow.Cells["nombre"].Value.ToString();
            txtApellidoPat.Text= dtgProf.CurrentRow.Cells["apellidoPaterno"].Value.ToString();
            txtApellidoMat.Text = dtgProf.CurrentRow.Cells["apellidoMaterno"].Value.ToString();
            txtDireccion.Text= dtgProf.CurrentRow.Cells["direccion"].Value.ToString();
            dtpFechan.Text= dtgProf.CurrentRow.Cells["fechanac"].Value.ToString();
            cmbCiudad.Text= dtgProf.CurrentRow.Cells["ciudad"].Value.ToString();
            cmbEstado.Text= dtgProf.CurrentRow.Cells["estado"].Value.ToString();
            txtNumeroCedula.Text = dtgProf.CurrentRow.Cells["numeroCedula"].Value.ToString();
            txtTitulo.Text= dtgProf.CurrentRow.Cells["titulo"].Value.ToString();
        }

        private void TextBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarProfesores(txtBuscar.Text);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            
            txtNombre.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            cargarProfesores();
            if (true)
            {
                try
                {
                GuardarProfesor();
                LimpiarCuadros();
                BuscarProfesores("");
                ControlarBotones(true, false, false, true);
                ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            
        }
    

        private void FrmProfesor_Load(object sender, EventArgs e)
        {
            EstadoManejador _estadomanejador = new EstadoManejador();
            _estadomanejador.GetEstadosList(cmbEstado);
            BuscarProfesores("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarProfesor();
                    BuscarProfesores("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void DtgProf_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void DtgProf_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarProfesor();
                BuscarProfesores("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           
        }

        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CiudadesManejador _ciudadesmanejador = new CiudadesManejador();
            _ciudadesmanejador.GetCiudadesList(cmbEstado.Text, cmbCiudad);
        }

        public void ultimoNum(string filtro)
        {
            comboBox1.DataSource = _profesormanejador.ultimoNumero(filtro);
            comboBox1.DisplayMember = "NextId";
            if (comboBox1.Text == "")
            {              
                    txtNumeroControl.Text = "D" + filtro + 1;
            }
            else
            {
                txtNumeroControl.Text = "D" + filtro + comboBox1.Text;             
            }
        }


      

        private void DtgañoIngreso_ValueChanged(object sender, EventArgs e)
        {
            ultimoNum(dtgañoIngreso.Value.Year.ToString());


        }

        private void Label12_Click(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            grpEstudios.Visible = true;
            txtBuscarEst.Text = " ";
            //txtMaest.Text = dtgProf.CurrentRow.Cells["nombre"].Value.ToString();
            txtMaestrod.Text = dtgProf.CurrentRow.Cells["nombre"].Value.ToString();
        }

        

        private void BtnCancelarEst_Click(object sender, EventArgs e)
        {
            grpAgregar.Visible = false;
            LimpiarCuadrosEst();
        }

        

        private void BtnnuevoEstudio_Click(object sender, EventArgs e)
        {
            txtMaest.Text = dtgProf.CurrentRow.Cells["nombre"].Value.ToString();
            txtMaestrod.Text = dtgProf.CurrentRow.Cells["nombre"].Value.ToString();
            grpAgregar.Visible = true;
            //lblIdEst.Text = "0";
            txtNombreEst.Focus();
            
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            grpEstudios.Visible = false;
            grpAgregar.Visible = false;
        }

        private void DtgProf_Click(object sender, EventArgs e)
        {
            
        }



        //]*************************************************************************************************************************

        private void CargarEstudios()
        {
            _estudios.Idestudio = Convert.ToInt32(lblIdEst.Text);
            _estudios.Nombreestudio = txtNombreEst.Text;
            _estudios.Documento = txtDocumento.Text;
            _estudios.Anoestudio = Convert.ToInt32(txtAño.Text);
            _estudios.Fkprofesor = int.Parse(lblId.Text);
        }

        private void LimpiarCuadrosEst()
        {
            txtNombreEst.Text = "";
            txtAño.Text = "";
            txtDocumento.Text = "";
            
            lblId.Text = "0";

        }

        private void BuscarEstudios(string filtro)
        {
            dtgEst.DataSource = _estudiosManejador.GetEstudios(filtro);
        }

        private void BtnGuardEst_Click(object sender, EventArgs e)
        {
            
            CargarEstudios();
            if (true)
            {
                try
                {
                    GuardarEstudio();
                    guardarPdf();
                    LimpiarCuadrosEst();
                    BuscarEstudios("");
                    
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            grpAgregar.Visible = false;
            //guardarPdf();
            
        }
        private void GuardarEstudio()
        {
            _estudiosManejador.Guardar(_estudios);

        }

        private void TxtBuscarEst_TextChanged(object sender, EventArgs e)
        {
            BuscarEstudios(lblId.Text);
        }

        private void EliminarEstudio()
        {
            var IdEstudio = dtgEst.CurrentRow.Cells["Idestudio"].Value;
            _estudiosManejador.Eliminar(Convert.ToInt32(IdEstudio));
        }

        private void ModificaEstudio()
        {
            txtMaest.Text = dtgProf.CurrentRow.Cells["nombre"].Value.ToString();
            txtMaestrod.Text = dtgProf.CurrentRow.Cells["nombre"].Value.ToString();
            lblIdEst.Text = dtgEst.CurrentRow.Cells["idestudio"].Value.ToString();
            txtNombreEst.Text = dtgEst.CurrentRow.Cells["nombreestudio"].Value.ToString();
            txtAño.Text = dtgEst.CurrentRow.Cells["anoestudio"].Value.ToString();
            txtDocumento.Text = dtgEst.CurrentRow.Cells["documento"].Value.ToString();
            lblId.Text = dtgEst.CurrentRow.Cells["fkprofesor"].Value.ToString();

        }
        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
            
        }

        private void BtnElimEst_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarEstudio();
                    BuscarEstudios("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void DgtEstudi_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ModificaEstudio();
            BuscarEstudios("");
            grpAgregar.Visible = true;

        }

        private void cargarPdf()
        {
            _ArchivoEstudios.Filter = "Archivo tipo (*.pdf)|*.pdf";
            _ArchivoEstudios.Title = "Cargar PDF";
            _ArchivoEstudios.ShowDialog();

            if (_ArchivoEstudios.FileName != "")
            {
                var archivo = new FileInfo(_ArchivoEstudios.FileName);

                txtDocumento.Text = archivo.Name;
            }
        }

        private void BtnCargarArchivo_Click(object sender, EventArgs e)
        {
            cargarPdf();
        }

        private void guardarPdf()
        {
            if (_ArchivoEstudios.FileName != null)
            {
                if (_ArchivoEstudios.FileName != "")
                {
                    var archivo = new FileInfo(_ArchivoEstudios.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");

                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                //archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }
            }
        }

        private void eliminarPDF()
        {
            if (MessageBox.Show("Estas seguro de eliminar el archivo", "Eliminar archivo",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {

                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");

                    FileInfo archivoAnterior;

                    if (obtenerArchivos.Length != 0)
                    {

                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }
                    }
                }
            }
        }

        private void BtnElimArchivo_Click(object sender, EventArgs e)
        {
            eliminarPDF();
        }
    }
}
