﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using System.IO;

namespace ControlEscolar2
{
    public partial class frmEstudios : Form
    {
        private OpenFileDialog _ArchivoEstudios;

        private EstudiosManejador _estudiosManejador;
        private Estudios _estudios;
        private string _ruta;

        public frmEstudios()
        {
            InitializeComponent();
            _estudiosManejador = new EstudiosManejador();
            _estudios = new Estudios();
            _ArchivoEstudios = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Archivos\\";
        }

        private void BtnCargarArchivo_Click(object sender, EventArgs e)
        {
            cargarPdf();
        
        }
        private void cargarPdf()
        {
            _ArchivoEstudios.Filter = "Archivo tipo (*.pdf)|*.pdf";
            _ArchivoEstudios.Title = "Cargar PDF";
            _ArchivoEstudios.ShowDialog();

            if (_ArchivoEstudios.FileName != "")
            {
                var archivo = new FileInfo(_ArchivoEstudios.FileName);

                txtDocumento.Text = archivo.Name;
            }
        }

        private void guardarPdf()
        {
            if (_ArchivoEstudios.FileName != null)
            {
                if (_ArchivoEstudios.FileName != "")
                {
                    var archivo = new FileInfo(_ArchivoEstudios.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");

                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }
            }
        }

        private void BtnElimArchivo_Click(object sender, EventArgs e)
        {
            eliminarPDF();
        }
        private void eliminarPDF()
        {
            if (MessageBox.Show("Estas seguro de eliminar el archivo", "Eliminar archivo",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {

                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");

                    FileInfo archivoAnterior;

                    if (obtenerArchivos.Length != 0)
                    {

                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }
                    }
                }
            }
        }

        private void CargarEstudios()
        {           
            _estudios.Idestudio = Convert.ToInt32(lblId.Text);
            _estudios.Nombreestudio= txtNombre.Text;
            _estudios.Documento = txtDocumento.Text;
            _estudios.Anoestudio = Convert.ToInt32(txtAño.Text);
            _estudios.Fkprofesor = 5;
        }

        private void FrmEstudios_Load(object sender, EventArgs e)
        {
            EstudiosManejador _estudiosManejador = new EstudiosManejador();
            _estudiosManejador.GetProfesoresLists(cmbProfesor);
            BuscarEstudios("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtAño.Enabled = activar;
            txtDocumento.Enabled = activar;
            cmbProfesor.Enabled = activar;
           
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtAño.Text = "";
            txtDocumento.Text = "";
            cmbProfesor.Text = "";
            lblId.Text = "0";
        
        }

        private void BuscarEstudios(string filtro)
        {
            dtgEstudios.DataSource = _estudiosManejador.GetEstudios(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtNombre.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            
            CargarEstudios();
            if (true)
            {
                try
                {
                    GuardarEstudio();
                    guardarPdf();
                    LimpiarCuadros();
                    BuscarEstudios("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void GuardarEstudio()
        {
            _estudiosManejador.Guardar(_estudios);

        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarEstudios(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarEstudio();
                    BuscarEstudios("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }


        private void EliminarEstudio()
        {
            var IdEstudio = dtgEstudios.CurrentRow.Cells["Idestudio"].Value;
            _estudiosManejador.Eliminar(Convert.ToInt32(IdEstudio));
        }

        private void ModificaEstudio()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgEstudios.CurrentRow.Cells["idestudio"].Value.ToString();
            txtNombre.Text = dtgEstudios.CurrentRow.Cells["nombreestudio"].Value.ToString();
            txtAño.Text = dtgEstudios.CurrentRow.Cells["anoestudio"].Value.ToString();
            txtDocumento.Text = dtgEstudios.CurrentRow.Cells["documento"].Value.ToString();
            cmbProfesor.Text = dtgEstudios.CurrentRow.Cells["fkprofesor"].Value.ToString();

        }

        private void DtgEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificaEstudio();
            BuscarEstudios("");
            

        }

        private void CmbProfesor_SelectedIndexChanged(object sender, EventArgs e)
        {
            EstudiosManejador _estudiosManejador = new EstudiosManejador();
            _estudiosManejador.GetProfesoresId(cmbProfesor.Text);
        }
    }
}
