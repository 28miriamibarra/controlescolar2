﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class frmAlumno : Form
    {

        private AlumnoManejador _alumnomanejador;
        private Alumno _alumno;

        public frmAlumno()
        {
            InitializeComponent();
            _alumnomanejador = new AlumnoManejador();
            _alumno = new Alumno();
        }

        private void CargarAlumno()
        {
            _alumno.Idalumno = Convert.ToInt32(lblId.Text);
            _alumno.Nombre = txtNombre.Text;
            _alumno.Apellidopaterno = txtAppat.Text;
            _alumno.Apellidomaterno = txtApeMat.Text;
            _alumno.Fechanac = Convert.ToDateTime(dtpFechan.Text);
            _alumno.Domicilio = txtDomicilio.Text;
            _alumno.Email = txtEmail.Text;
            _alumno.Sexo = cmbSexo.Text;
            _alumno.Numerocontrol = int.Parse(txtNcontrol.Text);
            _alumno.Estado = cmbEstado.Text;
            _alumno.Ciudad = cmbCiudad.Text;
        }


        private void FrmAlumno_Load(object sender, EventArgs e)
        {
            //cmbEstado.DataSource = _estadomanejador.GetEstadosList();
            //cmbEstado.DisplayMember = "nombre";
            EstadoManejador _estadomanejador = new EstadoManejador();
            _estadomanejador.GetEstadosList(cmbEstado);
            BuscarAlumnos("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtApeMat.Enabled = activar;
            txtAppat.Enabled = activar;
            txtNcontrol.Enabled = activar;
            txtEmail.Enabled = activar;
            dtpFechan.Enabled = activar;
            cmbSexo.Enabled = activar;
            cmbEstado.Enabled = activar;
            cmbCiudad.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApeMat.Text = "";
            txtAppat.Text = "";
            txtDomicilio.Text = "";
            
            txtEmail.Text = "";
            txtNcontrol.Text = "";
            dtpFechan.Text = "";
            cmbSexo.Text = "";
            lblId.Text = "0";
            cmbEstado.Text = "";
            cmbCiudad.Text = "";

        }
        private void BuscarAlumnos(string filtro)
        {
            dgvAlumno.DataSource = _alumnomanejador.GetAlumnos(filtro);
        }
        

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtNombre.Focus();
        }


        private bool ValidarAlumno()
        {
            var tupla = _alumnomanejador.ValidarAlumno(_alumno);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;

            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de validacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return valido;
        }


        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarAlumno();
            if (true)
            {
                try
                {
                    GuardarAlumno();
                    LimpiarCuadros();
                    BuscarAlumnos("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void GuardarAlumno()
        {
            _alumnomanejador.Guardar(_alumno);
            
        }
        

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumnos(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarAlumno();
                    BuscarAlumnos("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarAlumno()
        {
            var Idalumno = dgvAlumno.CurrentRow.Cells["Idalumno"].Value;
            _alumnomanejador.Eliminar(Convert.ToInt32(Idalumno));
        }

        private void DgvAlumno_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ModificarAlumno()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dgvAlumno.CurrentRow.Cells["idalumno"].Value.ToString();
            txtNombre.Text = dgvAlumno.CurrentRow.Cells["nombre"].Value.ToString();
            txtAppat.Text = dgvAlumno.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            txtApeMat.Text = dgvAlumno.CurrentRow.Cells["apellidomaterno"].Value.ToString();
            txtDomicilio.Text = dgvAlumno.CurrentRow.Cells["domicilio"].Value.ToString();
            txtEmail.Text = dgvAlumno.CurrentRow.Cells["email"].Value.ToString();
            txtNcontrol.Text = dgvAlumno.CurrentRow.Cells["numerocontrol"].Value.ToString();
            dtpFechan.Text = dgvAlumno.CurrentRow.Cells["fechanac"].Value.ToString();
            cmbSexo.Text = dgvAlumno.CurrentRow.Cells["sexo"].Value.ToString();
            cmbEstado.Text= dgvAlumno.CurrentRow.Cells["fkcodigoestado"].Value.ToString();
            cmbCiudad.Text= dgvAlumno.CurrentRow.Cells["fkciudad"].Value.ToString();
        }

        private void DgvAlumno_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
                ModificarAlumno();
                BuscarAlumnos("");
           
        }

 
        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CiudadesManejador _ciudadesmanejador = new CiudadesManejador();
            _ciudadesmanejador.GetCiudadesList(cmbEstado.Text, cmbCiudad);
        }

        private void Nuevo(object sender, EventArgs e)
        {

        }

        private void DgvAlumno_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TxtApeMat_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void CmbCiudad_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CmbSexo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void LblNombre_Click(object sender, EventArgs e)
        {

        }

        private void Label10_Click(object sender, EventArgs e)
        {

        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void TxtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtDomicilio_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label8_Click(object sender, EventArgs e)
        {

        }

        private void DtpFechan_ValueChanged(object sender, EventArgs e)
        {

        }

        private void TxtNcontrol_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void LblId_Click(object sender, EventArgs e)
        {

        }

        private void TxtAppat_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }
    }
}
