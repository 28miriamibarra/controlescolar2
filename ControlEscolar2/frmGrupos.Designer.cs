﻿namespace ControlEscolar2
{
    partial class frmGrupos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGrupos));
            this.dtgVista = new System.Windows.Forms.DataGridView();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.grpVista = new System.Windows.Forms.GroupBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnAlumnos = new System.Windows.Forms.Button();
            this.lblidAsignar = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNoControl = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNombreA = new System.Windows.Forms.TextBox();
            this.cmbGrupos = new System.Windows.Forms.ComboBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.cmbId = new System.Windows.Forms.ComboBox();
            this.cmbIDaL = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dtgAlumnos = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblId = new System.Windows.Forms.Label();
            this.grpEleg = new System.Windows.Forms.GroupBox();
            this.btnCanceLARaL = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgVista)).BeginInit();
            this.grpVista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpEleg.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgVista
            // 
            this.dtgVista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgVista.Location = new System.Drawing.Point(16, 86);
            this.dtgVista.Name = "dtgVista";
            this.dtgVista.Size = new System.Drawing.Size(732, 199);
            this.dtgVista.TabIndex = 0;
            this.dtgVista.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgVista_CellContentClick);
            this.dtgVista.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgVista_CellDoubleClick);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(12, 19);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(373, 20);
            this.txtBuscar.TabIndex = 1;
            this.txtBuscar.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // grpVista
            // 
            this.grpVista.Controls.Add(this.btnEliminar);
            this.grpVista.Controls.Add(this.btnNuevo);
            this.grpVista.Controls.Add(this.btnAlumnos);
            this.grpVista.Controls.Add(this.lblidAsignar);
            this.grpVista.Controls.Add(this.label12);
            this.grpVista.Controls.Add(this.label6);
            this.grpVista.Controls.Add(this.txtNoControl);
            this.grpVista.Controls.Add(this.label11);
            this.grpVista.Controls.Add(this.txtNombreA);
            this.grpVista.Controls.Add(this.cmbGrupos);
            this.grpVista.Controls.Add(this.dtgVista);
            this.grpVista.Controls.Add(this.btnGuardar);
            this.grpVista.Controls.Add(this.btnCancelar);
            this.grpVista.Controls.Add(this.cmbId);
            this.grpVista.Controls.Add(this.cmbIDaL);
            this.grpVista.Controls.Add(this.comboBox1);
            this.grpVista.Controls.Add(this.textBox1);
            this.grpVista.Location = new System.Drawing.Point(7, 18);
            this.grpVista.Name = "grpVista";
            this.grpVista.Size = new System.Drawing.Size(765, 309);
            this.grpVista.TabIndex = 2;
            this.grpVista.TabStop = false;
            this.grpVista.Text = "Asignar Grupo";
            this.grpVista.Enter += new System.EventHandler(this.GrpVista_Enter);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnEliminar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar.BackgroundImage")));
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.Location = new System.Drawing.Point(704, 17);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(42, 48);
            this.btnEliminar.TabIndex = 79;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNuevo.BackgroundImage")));
            this.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNuevo.Location = new System.Drawing.Point(542, 17);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(52, 48);
            this.btnNuevo.TabIndex = 78;
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // btnAlumnos
            // 
            this.btnAlumnos.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnAlumnos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAlumnos.BackgroundImage")));
            this.btnAlumnos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAlumnos.Location = new System.Drawing.Point(488, 16);
            this.btnAlumnos.Name = "btnAlumnos";
            this.btnAlumnos.Size = new System.Drawing.Size(50, 48);
            this.btnAlumnos.TabIndex = 74;
            this.btnAlumnos.UseVisualStyleBackColor = false;
            this.btnAlumnos.Click += new System.EventHandler(this.BtnAlumnos_Click);
            // 
            // lblidAsignar
            // 
            this.lblidAsignar.AutoSize = true;
            this.lblidAsignar.BackColor = System.Drawing.Color.Transparent;
            this.lblidAsignar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblidAsignar.ForeColor = System.Drawing.Color.Black;
            this.lblidAsignar.Location = new System.Drawing.Point(0, 43);
            this.lblidAsignar.Name = "lblidAsignar";
            this.lblidAsignar.Size = new System.Drawing.Size(18, 20);
            this.lblidAsignar.TabIndex = 75;
            this.lblidAsignar.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 73;
            this.label12.Text = "GRUPO";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(141, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 72;
            this.label6.Text = "NOMBRE ALUMNO";
            // 
            // txtNoControl
            // 
            this.txtNoControl.Location = new System.Drawing.Point(297, 44);
            this.txtNoControl.Name = "txtNoControl";
            this.txtNoControl.Size = new System.Drawing.Size(133, 20);
            this.txtNoControl.TabIndex = 69;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(296, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 71;
            this.label11.Text = "NO. CONTROL";
            // 
            // txtNombreA
            // 
            this.txtNombreA.Location = new System.Drawing.Point(138, 44);
            this.txtNombreA.Name = "txtNombreA";
            this.txtNombreA.Size = new System.Drawing.Size(148, 20);
            this.txtNombreA.TabIndex = 70;
            // 
            // cmbGrupos
            // 
            this.cmbGrupos.FormattingEnabled = true;
            this.cmbGrupos.Location = new System.Drawing.Point(18, 44);
            this.cmbGrupos.Name = "cmbGrupos";
            this.cmbGrupos.Size = new System.Drawing.Size(108, 21);
            this.cmbGrupos.TabIndex = 1;
            this.cmbGrupos.SelectedIndexChanged += new System.EventHandler(this.CmbGrupos_SelectedIndexChanged);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardar.BackgroundImage")));
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardar.Location = new System.Drawing.Point(600, 17);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(46, 48);
            this.btnGuardar.TabIndex = 57;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(652, 17);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(46, 48);
            this.btnCancelar.TabIndex = 58;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // cmbId
            // 
            this.cmbId.FormattingEnabled = true;
            this.cmbId.Location = new System.Drawing.Point(544, 27);
            this.cmbId.Name = "cmbId";
            this.cmbId.Size = new System.Drawing.Size(28, 21);
            this.cmbId.TabIndex = 76;
            // 
            // cmbIDaL
            // 
            this.cmbIDaL.FormattingEnabled = true;
            this.cmbIDaL.Location = new System.Drawing.Point(600, 29);
            this.cmbIDaL.Name = "cmbIDaL";
            this.cmbIDaL.Size = new System.Drawing.Size(29, 21);
            this.cmbIDaL.TabIndex = 80;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(21, 86);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(32, 21);
            this.comboBox1.TabIndex = 82;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(100, 87);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(26, 20);
            this.textBox1.TabIndex = 81;
            // 
            // dtgAlumnos
            // 
            this.dtgAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAlumnos.Location = new System.Drawing.Point(12, 64);
            this.dtgAlumnos.Name = "dtgAlumnos";
            this.dtgAlumnos.Size = new System.Drawing.Size(744, 221);
            this.dtgAlumnos.TabIndex = 3;
            this.dtgAlumnos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAlumnos_CellContentClick);
            this.dtgAlumnos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAlumnos_CellDoubleClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(391, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 73;
            this.pictureBox1.TabStop = false;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.ForeColor = System.Drawing.Color.Red;
            this.lblId.Location = new System.Drawing.Point(672, 27);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(29, 31);
            this.lblId.TabIndex = 63;
            this.lblId.Text = "0";
            // 
            // grpEleg
            // 
            this.grpEleg.Controls.Add(this.btnCanceLARaL);
            this.grpEleg.Controls.Add(this.dtgAlumnos);
            this.grpEleg.Controls.Add(this.lblId);
            this.grpEleg.Controls.Add(this.pictureBox1);
            this.grpEleg.Controls.Add(this.txtBuscar);
            this.grpEleg.Location = new System.Drawing.Point(16, 18);
            this.grpEleg.Name = "grpEleg";
            this.grpEleg.Size = new System.Drawing.Size(750, 315);
            this.grpEleg.TabIndex = 75;
            this.grpEleg.TabStop = false;
            this.grpEleg.Text = "Elegir Alumno";
            this.grpEleg.Enter += new System.EventHandler(this.GrpEleg_Enter);
            // 
            // btnCanceLARaL
            // 
            this.btnCanceLARaL.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCanceLARaL.BackgroundImage")));
            this.btnCanceLARaL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCanceLARaL.Location = new System.Drawing.Point(710, 10);
            this.btnCanceLARaL.Name = "btnCanceLARaL";
            this.btnCanceLARaL.Size = new System.Drawing.Size(46, 48);
            this.btnCanceLARaL.TabIndex = 81;
            this.btnCanceLARaL.UseVisualStyleBackColor = true;
            this.btnCanceLARaL.Click += new System.EventHandler(this.BtnCanceLARaL_Click);
            // 
            // frmGrupos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(790, 345);
            this.Controls.Add(this.grpVista);
            this.Controls.Add(this.grpEleg);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGrupos";
            this.Text = "frmGrupos";
            this.Load += new System.EventHandler(this.FrmGrupos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgVista)).EndInit();
            this.grpVista.ResumeLayout(false);
            this.grpVista.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpEleg.ResumeLayout(false);
            this.grpEleg.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgVista;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.GroupBox grpVista;
        private System.Windows.Forms.DataGridView dtgAlumnos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAlumnos;
        private System.Windows.Forms.ComboBox cmbGrupos;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNoControl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNombreA;
        private System.Windows.Forms.Label lblidAsignar;
        private System.Windows.Forms.ComboBox cmbId;
        private System.Windows.Forms.GroupBox grpEleg;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.ComboBox cmbIDaL;
        private System.Windows.Forms.Button btnCanceLARaL;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}