﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class frmAgregar_Grupod : Form
    {
        GruposidManejador _gruposidManejador = new GruposidManejador();
        Gruposid _grup = new Gruposid();

        public frmAgregar_Grupod()
        {
            InitializeComponent();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtnombr.Enabled = activar;
            txtNombreGrupo.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            txtNombreGrupo.Text = "";
            txtnombr.Text = "";           
            lblId.Text = "0";

        }

        private void FrmAgregar_Grupod_Load(object sender, EventArgs e)
        {
            BuscarGrupos("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();

        }
        private void CargarGrupos()
        {
            _grup.IdGrupo = Convert.ToInt32(lblId.Text);
            _grup.Nombregrupo = txtNombreGrupo.Text;
            _grup.Nomb = txtnombr.Text;
            
        }

        public void BuscarGrupos(string filtro)
        {
            dtgGrupo.DataSource = _gruposidManejador.GetGruposids(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtnombr.Focus();
        }
        private void GuardarGrupo()
        {
            _gruposidManejador.Guardar(_grup);

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarGrupos();
            if (true)
            {
                try
                {
                    GuardarGrupo();
                    LimpiarCuadros();
                    BuscarGrupos("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGrupos(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void EliminarGrupo()
        {
            var IdGrupo = dtgGrupo.CurrentRow.Cells["IdGrupo"].Value;
            _gruposidManejador.Eliminar(Convert.ToInt32(IdGrupo));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarGrupo();
                    BuscarGrupos("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ModificaEstudio()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgGrupo.CurrentRow.Cells["idgrupo"].Value.ToString();
            txtNombreGrupo.Text = dtgGrupo.CurrentRow.Cells["nombregrupo"].Value.ToString();
            txtnombr.Text = dtgGrupo.CurrentRow.Cells["nomb"].Value.ToString();

        }

        private void DtgGrupo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificaEstudio();
            BuscarGrupos("");
           
        }
    }
}
