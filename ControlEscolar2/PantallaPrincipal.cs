﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar2
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuario usuario = new frmUsuario();
            usuario.ShowDialog();
        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlumno alumno = new frmAlumno();
            alumno.ShowDialog();

        }

        private void ProfesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfesor profesor = new frmProfesor();
            profesor.ShowDialog();
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void EstudiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEstudios estudios = new frmEstudios();
            estudios.ShowDialog();
        }

        private void MateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMaterias materias = new frmMaterias();
            materias.ShowDialog();
        }

        private void EscuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEscuela escuela = new frmEscuela();
            escuela.ShowDialog();
        }

        private void GruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGrupos grupos = new frmGrupos();
            grupos.ShowDialog();
        }

        private void AsignarMateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAsignarMateria asignarMateria = new frmAsignarMateria();
            asignarMateria.ShowDialog();
        }

        private void CalificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCalificaciones frmCalificaciones = new frmCalificaciones();
            frmCalificaciones.ShowDialog();
        }

        private void EditarGruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAgregar_Grupod frmAgregar_Grupod = new frmAgregar_Grupod();
            frmAgregar_Grupod.ShowDialog();
        }
    }
}
