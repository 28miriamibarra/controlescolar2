﻿namespace ControlEscolar2
{
    partial class frmProfesor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfesor));
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumeroControl = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtApellidoPat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtApellidoMat = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpFechan = new System.Windows.Forms.DateTimePicker();
            this.cmbCiudad = new System.Windows.Forms.ComboBox();
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumeroCedula = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtgProf = new System.Windows.Forms.DataGridView();
            this.dtgañoIngreso = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnEstudios = new System.Windows.Forms.Button();
            this.grpEstudios = new System.Windows.Forms.GroupBox();
            this.lblIdEst = new System.Windows.Forms.Label();
            this.grpAgregar = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnGuardEst = new System.Windows.Forms.Button();
            this.txtMaest = new System.Windows.Forms.TextBox();
            this.txtBuscarEst = new System.Windows.Forms.TextBox();
            this.txtAño = new System.Windows.Forms.TextBox();
            this.btnElimArchivo = new System.Windows.Forms.Button();
            this.btnCargarArchivo = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNombreEst = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDocumento = new System.Windows.Forms.TextBox();
            this.btnCancelarEst = new System.Windows.Forms.Button();
            this.btnElimEst = new System.Windows.Forms.Button();
            this.btnnuevoEstudio = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.dtgEst = new System.Windows.Forms.DataGridView();
            this.txtMaestrod = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProf)).BeginInit();
            this.grpEstudios.SuspendLayout();
            this.grpAgregar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEst)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnGuardar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardar.BackgroundImage")));
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardar.Location = new System.Drawing.Point(569, -1);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(64, 60);
            this.btnGuardar.TabIndex = 16;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(639, -1);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(55, 60);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnEliminar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar.BackgroundImage")));
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.Location = new System.Drawing.Point(700, -1);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(55, 59);
            this.btnEliminar.TabIndex = 14;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNuevo.BackgroundImage")));
            this.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNuevo.Location = new System.Drawing.Point(496, -3);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(67, 65);
            this.btnNuevo.TabIndex = 13;
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(363, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(46, 46);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(25, 20);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(335, 20);
            this.txtBuscar.TabIndex = 1;
            this.txtBuscar.TextChanged += new System.EventHandler(this.TxtBuscar_TextChanged);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.ForeColor = System.Drawing.Color.Red;
            this.lblId.Location = new System.Drawing.Point(453, 22);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(23, 25);
            this.lblId.TabIndex = 20;
            this.lblId.Text = "0";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(23, 174);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(225, 20);
            this.txtNombre.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "NOMBRE";
            // 
            // txtNumeroControl
            // 
            this.txtNumeroControl.Enabled = false;
            this.txtNumeroControl.Location = new System.Drawing.Point(23, 129);
            this.txtNumeroControl.Name = "txtNumeroControl";
            this.txtNumeroControl.Size = new System.Drawing.Size(225, 20);
            this.txtNumeroControl.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "NUMERO DE CONTROL";
            // 
            // txtApellidoPat
            // 
            this.txtApellidoPat.Location = new System.Drawing.Point(264, 174);
            this.txtApellidoPat.Name = "txtApellidoPat";
            this.txtApellidoPat.Size = new System.Drawing.Size(225, 20);
            this.txtApellidoPat.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(263, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "APELLIDO PATERNO";
            // 
            // txtApellidoMat
            // 
            this.txtApellidoMat.Location = new System.Drawing.Point(500, 83);
            this.txtApellidoMat.Name = "txtApellidoMat";
            this.txtApellidoMat.Size = new System.Drawing.Size(245, 20);
            this.txtApellidoMat.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(499, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "APELLIDO MATERNO";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(23, 217);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(466, 20);
            this.txtDireccion.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "DIRECCION";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(501, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "FECHA DE NACIMIENTO";
            // 
            // dtpFechan
            // 
            this.dtpFechan.Location = new System.Drawing.Point(500, 129);
            this.dtpFechan.Name = "dtpFechan";
            this.dtpFechan.Size = new System.Drawing.Size(245, 20);
            this.dtpFechan.TabIndex = 8;
            // 
            // cmbCiudad
            // 
            this.cmbCiudad.FormattingEnabled = true;
            this.cmbCiudad.Location = new System.Drawing.Point(500, 217);
            this.cmbCiudad.Name = "cmbCiudad";
            this.cmbCiudad.Size = new System.Drawing.Size(245, 21);
            this.cmbCiudad.TabIndex = 10;
            // 
            // cmbEstado
            // 
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Location = new System.Drawing.Point(500, 173);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(245, 21);
            this.cmbEstado.TabIndex = 9;
            this.cmbEstado.SelectedIndexChanged += new System.EventHandler(this.CmbEstado_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(497, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "CIUDAD";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(501, 159);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "ESTADO";
            // 
            // txtNumeroCedula
            // 
            this.txtNumeroCedula.Location = new System.Drawing.Point(264, 129);
            this.txtNumeroCedula.Name = "txtNumeroCedula";
            this.txtNumeroCedula.Size = new System.Drawing.Size(225, 20);
            this.txtNumeroCedula.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(263, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 50;
            this.label9.Text = "NUMERO CEDULA";
            // 
            // txtTitulo
            // 
            this.txtTitulo.Location = new System.Drawing.Point(264, 85);
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.Size = new System.Drawing.Size(225, 20);
            this.txtTitulo.TabIndex = 11;
            this.txtTitulo.TextChanged += new System.EventHandler(this.TextBox6_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(263, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 52;
            this.label10.Text = "TITULO";
            // 
            // dtgProf
            // 
            this.dtgProf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProf.Location = new System.Drawing.Point(23, 295);
            this.dtgProf.Name = "dtgProf";
            this.dtgProf.Size = new System.Drawing.Size(726, 155);
            this.dtgProf.TabIndex = 54;
            this.dtgProf.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgProf_CellContentClick);
            this.dtgProf.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgProf_CellDoubleClick);
            this.dtgProf.Click += new System.EventHandler(this.DtgProf_Click);
            // 
            // dtgañoIngreso
            // 
            this.dtgañoIngreso.Location = new System.Drawing.Point(23, 85);
            this.dtgañoIngreso.Name = "dtgañoIngreso";
            this.dtgañoIngreso.Size = new System.Drawing.Size(225, 20);
            this.dtgañoIngreso.TabIndex = 56;
            this.dtgañoIngreso.ValueChanged += new System.EventHandler(this.DtgañoIngreso_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 57;
            this.label11.Text = "FECHA DE INGRESO";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(769, 140);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(26, 21);
            this.comboBox1.TabIndex = 62;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // btnEstudios
            // 
            this.btnEstudios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEstudios.BackgroundImage")));
            this.btnEstudios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEstudios.Location = new System.Drawing.Point(691, 251);
            this.btnEstudios.Name = "btnEstudios";
            this.btnEstudios.Size = new System.Drawing.Size(54, 33);
            this.btnEstudios.TabIndex = 63;
            this.btnEstudios.UseVisualStyleBackColor = true;
            this.btnEstudios.Click += new System.EventHandler(this.Button1_Click);
            // 
            // grpEstudios
            // 
            this.grpEstudios.BackColor = System.Drawing.Color.AliceBlue;
            this.grpEstudios.Controls.Add(this.lblIdEst);
            this.grpEstudios.Controls.Add(this.btnElimEst);
            this.grpEstudios.Controls.Add(this.btnnuevoEstudio);
            this.grpEstudios.Controls.Add(this.btnCerrar);
            this.grpEstudios.Controls.Add(this.dtgEst);
            this.grpEstudios.Controls.Add(this.txtMaestrod);
            this.grpEstudios.Controls.Add(this.label13);
            this.grpEstudios.Location = new System.Drawing.Point(90, 88);
            this.grpEstudios.Name = "grpEstudios";
            this.grpEstudios.Size = new System.Drawing.Size(595, 292);
            this.grpEstudios.TabIndex = 64;
            this.grpEstudios.TabStop = false;
            this.grpEstudios.Text = "Estudios";
            // 
            // lblIdEst
            // 
            this.lblIdEst.AutoSize = true;
            this.lblIdEst.Location = new System.Drawing.Point(373, 22);
            this.lblIdEst.Name = "lblIdEst";
            this.lblIdEst.Size = new System.Drawing.Size(13, 13);
            this.lblIdEst.TabIndex = 76;
            this.lblIdEst.Text = "0";
            // 
            // grpAgregar
            // 
            this.grpAgregar.Controls.Add(this.label12);
            this.grpAgregar.Controls.Add(this.btnGuardEst);
            this.grpAgregar.Controls.Add(this.txtMaest);
            this.grpAgregar.Controls.Add(this.txtBuscarEst);
            this.grpAgregar.Controls.Add(this.txtAño);
            this.grpAgregar.Controls.Add(this.btnElimArchivo);
            this.grpAgregar.Controls.Add(this.btnCargarArchivo);
            this.grpAgregar.Controls.Add(this.label14);
            this.grpAgregar.Controls.Add(this.txtNombreEst);
            this.grpAgregar.Controls.Add(this.label15);
            this.grpAgregar.Controls.Add(this.label16);
            this.grpAgregar.Controls.Add(this.txtDocumento);
            this.grpAgregar.Controls.Add(this.btnCancelarEst);
            this.grpAgregar.Location = new System.Drawing.Point(90, 109);
            this.grpAgregar.Name = "grpAgregar";
            this.grpAgregar.Size = new System.Drawing.Size(586, 224);
            this.grpAgregar.TabIndex = 67;
            this.grpAgregar.TabStop = false;
            this.grpAgregar.Text = "Modificar";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 76;
            this.label12.Text = "PROFESOR";
            // 
            // btnGuardEst
            // 
            this.btnGuardEst.BackColor = System.Drawing.Color.AliceBlue;
            this.btnGuardEst.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardEst.BackgroundImage")));
            this.btnGuardEst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardEst.Location = new System.Drawing.Point(426, 16);
            this.btnGuardEst.Name = "btnGuardEst";
            this.btnGuardEst.Size = new System.Drawing.Size(64, 60);
            this.btnGuardEst.TabIndex = 65;
            this.btnGuardEst.UseVisualStyleBackColor = false;
            this.btnGuardEst.Click += new System.EventHandler(this.BtnGuardEst_Click);
            // 
            // txtMaest
            // 
            this.txtMaest.Location = new System.Drawing.Point(12, 51);
            this.txtMaest.Name = "txtMaest";
            this.txtMaest.Size = new System.Drawing.Size(170, 20);
            this.txtMaest.TabIndex = 68;
            // 
            // txtBuscarEst
            // 
            this.txtBuscarEst.Location = new System.Drawing.Point(489, 17);
            this.txtBuscarEst.Name = "txtBuscarEst";
            this.txtBuscarEst.Size = new System.Drawing.Size(10, 20);
            this.txtBuscarEst.TabIndex = 69;
            this.txtBuscarEst.TextChanged += new System.EventHandler(this.TxtBuscarEst_TextChanged);
            // 
            // txtAño
            // 
            this.txtAño.Location = new System.Drawing.Point(15, 152);
            this.txtAño.Name = "txtAño";
            this.txtAño.Size = new System.Drawing.Size(264, 20);
            this.txtAño.TabIndex = 75;
            // 
            // btnElimArchivo
            // 
            this.btnElimArchivo.BackColor = System.Drawing.Color.Crimson;
            this.btnElimArchivo.Location = new System.Drawing.Point(522, 105);
            this.btnElimArchivo.Name = "btnElimArchivo";
            this.btnElimArchivo.Size = new System.Drawing.Size(25, 23);
            this.btnElimArchivo.TabIndex = 74;
            this.btnElimArchivo.Text = "X";
            this.btnElimArchivo.UseVisualStyleBackColor = false;
            this.btnElimArchivo.Click += new System.EventHandler(this.BtnElimArchivo_Click);
            // 
            // btnCargarArchivo
            // 
            this.btnCargarArchivo.BackColor = System.Drawing.Color.LightBlue;
            this.btnCargarArchivo.Location = new System.Drawing.Point(491, 104);
            this.btnCargarArchivo.Name = "btnCargarArchivo";
            this.btnCargarArchivo.Size = new System.Drawing.Size(25, 24);
            this.btnCargarArchivo.TabIndex = 73;
            this.btnCargarArchivo.Text = "...";
            this.btnCargarArchivo.UseVisualStyleBackColor = false;
            this.btnCargarArchivo.Click += new System.EventHandler(this.BtnCargarArchivo_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 13);
            this.label14.TabIndex = 70;
            this.label14.Text = "AÑO DE TITULACION";
            // 
            // txtNombreEst
            // 
            this.txtNombreEst.Location = new System.Drawing.Point(13, 108);
            this.txtNombreEst.Name = "txtNombreEst";
            this.txtNombreEst.Size = new System.Drawing.Size(266, 20);
            this.txtNombreEst.TabIndex = 68;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 95);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 69;
            this.label15.Text = "ESTUDIO";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(316, 91);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(137, 13);
            this.label16.TabIndex = 67;
            this.label16.Text = "ADJUNTAR TITULO (PDF)";
            // 
            // txtDocumento
            // 
            this.txtDocumento.Location = new System.Drawing.Point(307, 107);
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.Size = new System.Drawing.Size(178, 20);
            this.txtDocumento.TabIndex = 66;
            // 
            // btnCancelarEst
            // 
            this.btnCancelarEst.BackColor = System.Drawing.Color.AliceBlue;
            this.btnCancelarEst.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelarEst.BackgroundImage")));
            this.btnCancelarEst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarEst.Location = new System.Drawing.Point(496, 16);
            this.btnCancelarEst.Name = "btnCancelarEst";
            this.btnCancelarEst.Size = new System.Drawing.Size(55, 60);
            this.btnCancelarEst.TabIndex = 65;
            this.btnCancelarEst.UseVisualStyleBackColor = false;
            this.btnCancelarEst.Click += new System.EventHandler(this.BtnCancelarEst_Click);
            // 
            // btnElimEst
            // 
            this.btnElimEst.BackColor = System.Drawing.Color.AliceBlue;
            this.btnElimEst.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnElimEst.BackgroundImage")));
            this.btnElimEst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnElimEst.Location = new System.Drawing.Point(464, 12);
            this.btnElimEst.Name = "btnElimEst";
            this.btnElimEst.Size = new System.Drawing.Size(55, 59);
            this.btnElimEst.TabIndex = 65;
            this.btnElimEst.UseVisualStyleBackColor = false;
            this.btnElimEst.Click += new System.EventHandler(this.BtnElimEst_Click);
            // 
            // btnnuevoEstudio
            // 
            this.btnnuevoEstudio.BackColor = System.Drawing.Color.AliceBlue;
            this.btnnuevoEstudio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnnuevoEstudio.BackgroundImage")));
            this.btnnuevoEstudio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnnuevoEstudio.Location = new System.Drawing.Point(391, 12);
            this.btnnuevoEstudio.Name = "btnnuevoEstudio";
            this.btnnuevoEstudio.Size = new System.Drawing.Size(67, 60);
            this.btnnuevoEstudio.TabIndex = 14;
            this.btnnuevoEstudio.UseVisualStyleBackColor = false;
            this.btnnuevoEstudio.Click += new System.EventHandler(this.BtnnuevoEstudio_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.Crimson;
            this.btnCerrar.Location = new System.Drawing.Point(526, 12);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(36, 23);
            this.btnCerrar.TabIndex = 1;
            this.btnCerrar.Text = "X";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // dtgEst
            // 
            this.dtgEst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEst.Location = new System.Drawing.Point(16, 95);
            this.dtgEst.Name = "dtgEst";
            this.dtgEst.Size = new System.Drawing.Size(536, 128);
            this.dtgEst.TabIndex = 0;
            this.dtgEst.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            this.dtgEst.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgtEstudi_CellMouseDoubleClick);
            // 
            // txtMaestrod
            // 
            this.txtMaestrod.Location = new System.Drawing.Point(15, 39);
            this.txtMaestrod.Name = "txtMaestrod";
            this.txtMaestrod.Size = new System.Drawing.Size(154, 20);
            this.txtMaestrod.TabIndex = 77;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 13);
            this.label13.TabIndex = 77;
            this.label13.Text = "PROFESOR";
            // 
            // frmProfesor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(763, 473);
            this.Controls.Add(this.grpEstudios);
            this.Controls.Add(this.grpAgregar);
            this.Controls.Add(this.btnEstudios);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.dtgañoIngreso);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dtgProf);
            this.Controls.Add(this.txtTitulo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtNumeroCedula);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.cmbCiudad);
            this.Controls.Add(this.cmbEstado);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dtpFechan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtApellidoMat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtApellidoPat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNumeroControl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnNuevo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmProfesor";
            this.Text = "frmProfesor";
            this.Load += new System.EventHandler(this.FrmProfesor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProf)).EndInit();
            this.grpEstudios.ResumeLayout(false);
            this.grpEstudios.PerformLayout();
            this.grpAgregar.ResumeLayout(false);
            this.grpAgregar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEst)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumeroControl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtApellidoPat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtApellidoMat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpFechan;
        private System.Windows.Forms.ComboBox cmbCiudad;
        private System.Windows.Forms.ComboBox cmbEstado;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumeroCedula;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTitulo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dtgProf;
        private System.Windows.Forms.DateTimePicker dtgañoIngreso;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnEstudios;
        private System.Windows.Forms.GroupBox grpEstudios;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.DataGridView dtgEst;
        private System.Windows.Forms.Button btnCancelarEst;
        private System.Windows.Forms.Button btnElimEst;
        private System.Windows.Forms.Button btnnuevoEstudio;
        private System.Windows.Forms.GroupBox grpAgregar;
        private System.Windows.Forms.TextBox txtAño;
        private System.Windows.Forms.Button btnElimArchivo;
        private System.Windows.Forms.Button btnCargarArchivo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNombreEst;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDocumento;
        private System.Windows.Forms.Button btnGuardEst;
        private System.Windows.Forms.TextBox txtMaest;
        private System.Windows.Forms.TextBox txtBuscarEst;
        private System.Windows.Forms.Label lblIdEst;
        private System.Windows.Forms.TextBox txtMaestrod;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}