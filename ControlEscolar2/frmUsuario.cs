﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using Microsoft.Office.Interop.Excel;


namespace ControlEscolar2
{
    public partial class frmUsuario : Form
    {
        private UsuarioManejador _usuarioManejador;
        private Usuario _usuario;

        
        public frmUsuario()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuario = new Usuario();
            
        }
        
        

        private void CargarUsuario()
        {
            _usuario.Idusuario = Convert.ToInt32(lblId.Text);
            _usuario.Nombre = txtNombre.Text;
            _usuario.App = txtAppat.Text;
            _usuario.Apm = txtApeMat.Text;
            _usuario.Contrasenia = txtContraseña.Text;
        }

        

        private void FrmUsuario_Load(object sender, EventArgs e)
        {
            BuscarUsuarios("");
            ControlarBotones(true,false,false,true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }

        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtContraseña.Enabled = activar;
            txtApeMat.Enabled = activar;
            txtAppat.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApeMat.Text = "";
            txtAppat.Text = "";
            txtContraseña.Text = "";
            lblId.Text = "0";
            
            
        }

        private void BuscarUsuarios(string filtro)
        {
            dgvUsuarios.DataSource = _usuarioManejador.GetUsuarios(filtro);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false,true,true,false);
            ControlarCuadros(true);

            txtNombre.Focus();


        }
        private bool ValidarUsuario()
        {
            var tupla = _usuarioManejador.ValidarUsuario(_usuario);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;

            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de validacion",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return valido;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
           CargarUsuario();
            if (true)
            {         
                GuardarUsuario();
                LimpiarCuadros();
                BuscarUsuarios("");
                ControlarBotones(true, false, false, true);
                ControlarCuadros(false);
            }

        }

        private void GuardarUsuario()
        {
            _usuarioManejador.Guardar(_usuario);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarUsuarios(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?","Eliminar registro", MessageBoxButtons.YesNo)==DialogResult.Yes);
            {
                try
                {
                    EliminarUsuario();
                    BuscarUsuarios("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarUsuario()
        {
            var IdUsuario = dgvUsuarios.CurrentRow.Cells["Idusuario"].Value;
            _usuarioManejador.Eliminar(Convert.ToInt32( IdUsuario));
        }

        private void DgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarUsuario();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
         
        private void ModificarUsuario()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dgvUsuarios.CurrentRow.Cells["Idusuario"].Value.ToString();
            txtNombre.Text = dgvUsuarios.CurrentRow.Cells["Nombre"].Value.ToString();
            txtAppat.Text = dgvUsuarios.CurrentRow.Cells["App"].Value.ToString();
            txtApeMat.Text = dgvUsuarios.CurrentRow.Cells["Apm"].Value.ToString();
            txtContraseña.Text = dgvUsuarios.CurrentRow.Cells["Contrasenia"].Value.ToString();
        }

        private void DgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void ExportarDataGridViewExcel(DataGridView grd)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog()== DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                   libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo =
                      (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //Recorremos el dataGridView rellenando la hoja

                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }

                    libros_trabajo.SaveAs(fichero.FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();

                    MessageBox.Show("Reporte Terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Fallo la creacion del archivo intenta de nuevo con otro nombre", "Error de creacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcel(dgvUsuarios);
        }

        private void BtnExportardllex_Click(object sender, EventArgs e)
        {
            
        }

        

        private void BtnPdf_Click(object sender, EventArgs e)
        {
            
        }
    }
}
