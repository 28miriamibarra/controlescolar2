﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
   
    public partial class frmMaterias : Form
    {
        private MateriasManejador _materiasManejador;
        private Materias _materias;

        public frmMaterias()
        {
            InitializeComponent();
            _materiasManejador  = new MateriasManejador();
            _materias = new Materias();
            
        }

        private void FrmMaterias_Load(object sender, EventArgs e)
        {
            BuscarMaterias("");
            
        }

        private void CargarMaterias()
        {
            _materias.Llave = Convert.ToInt32(lblId.Text);
            _materias.Idmateria = txtId.Text;
            _materias.Nombremateria = txtNombre.Text;
            _materias.Numhrsteoria =Convert.ToInt32( txtHoraTeoria.Text);
            _materias.Numhrspractica = Convert.ToInt32(txtHoraPract.Text);
            _materias.Creditos = Convert.ToInt32(lblCreditos.Text);
            _materias.Semestre = cmbSemestre.Text;
            _materias.Materiaant = txtAnterior.Text;
            _materias.Materiasig = txtSiguiente.Text;
        }

        private void BuscarMaterias(string filtro)
        {
            dtgMateria.DataSource = _materiasManejador.GetMaterias(filtro);
        }

        private void GuardarMateria()
        {
            _materiasManejador.Guardar(_materias);

        }

        

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            CargarMaterias();
            if (true)
            {
                try
                {
                    GuardarMateria();
                    //LimpiarCuadros();
                    BuscarMaterias("");
                    //ControlarBotones(true, false, false, true);
                    //ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaterias(txtBuscar.Text);
        }

        private void EliminarMateria()
        {
            string Idmateria = dtgMateria.CurrentRow.Cells["Llave"].Value.ToString();
            _materiasManejador.Eliminar(Idmateria);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarMateria();
                    BuscarMaterias("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ModificarMaterias()
        {
            //ControlarCuadros(true);
            //ControlarBotones(false, true, true, false);
            

            lblId.Text = dtgMateria.CurrentRow.Cells["llave"].Value.ToString();
            txtId.Text= dtgMateria.CurrentRow.Cells["idmateria"].Value.ToString();
            txtNombre.Text = dtgMateria.CurrentRow.Cells["nombremateria"].Value.ToString();
            txtHoraTeoria.Text = dtgMateria.CurrentRow.Cells["numhrsteoria"].Value.ToString();
            txtHoraPract.Text = dtgMateria.CurrentRow.Cells["numhrspractica"].Value.ToString();
            lblCreditos.Text = dtgMateria.CurrentRow.Cells["creditos"].Value.ToString();
            cmbSemestre.Text = dtgMateria.CurrentRow.Cells["semestre"].Value.ToString();
            txtAnterior.Text = dtgMateria.CurrentRow.Cells["materiaant"].Value.ToString();
            txtSiguiente.Text = dtgMateria.CurrentRow.Cells["materiasig"].Value.ToString();

        }

        private void LblCreditos_ChangeUICues(object sender, UICuesEventArgs e)
        {

        }

        private void TxtHoraPract_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int hrst = int.Parse(txtHoraTeoria.Text);
                int hrsp = int.Parse(txtHoraPract.Text);
                int cred = hrst + hrsp;
                lblCreditos.Text = cred.ToString();
            }
            catch(Exception)
            {
                MessageBox.Show("Es obligatorio llenar este campo");
            }
        }

        private void TxtHoraTeoria_TextChanged(object sender, EventArgs e)
        {
            
            try
            {
                int hrst = int.Parse(txtHoraTeoria.Text);
                lblCreditos.Text = hrst.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Es obligatorio llenar este campo");
            }
        }

        private void DtgMateria_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DtgMateria_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarMaterias();
            BuscarMaterias("");
        }

        private void CmbSemestre_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {

        }
    }
}
