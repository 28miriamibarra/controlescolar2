﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Windows.Forms;


namespace LogicaNegocios.ControlEscolar2
{
    public class AsignarMateriaManejador
    {
        AsignarMateriasAccesoDatos _asignarMaterias = new AsignarMateriasAccesoDatos();
        ProfesorAccesoDatos _profesorAccesoDatos = new ProfesorAccesoDatos();

        public void Guardar(AsignarMateria materias)
        {
            _asignarMaterias.Guardar(materias);
        }

        public void Eliminar(string idmateria)
        {
            _asignarMaterias.Eliminar(idmateria);
        }

        public List<AsignarMateria> GetMaterias(string filtro)
        {
            var listamaterias = _asignarMaterias.GetAsignars(filtro);
            return listamaterias;
        }
        public List<Profesores> GetProfesoresLists(ComboBox cm)
        {
            var listProfesores = new List<Profesores>();
            listProfesores = _profesorAccesoDatos.GetProfesoresLists(cm);
            return listProfesores;
        }
        public List<AsignarMateria> GetMateriasnom(ComboBox cm)
        {
            var listProfesores = new List<AsignarMateria>();
            listProfesores = _asignarMaterias.GetMateriasnom(cm);
            return listProfesores;
        }


    }
}
