﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class CalificacionesManejador
    {
        CalificacionesAccesoDatos _calificacionesAccesoDatos = new CalificacionesAccesoDatos();

        public void Guardar(Calificaciones parcial)
        {
            _calificacionesAccesoDatos.Guardar(parcial);
        }


        public List<Alumno> GetIdAlumno (string filtro, ComboBox cm)
        {
            var listProfesores = new List<Alumno>();
            listProfesores = _calificacionesAccesoDatos.getidalumno(filtro, cm);
            return listProfesores;
        }

        public List<AsignarMateria> GetIDList(string filtro, ComboBox cm)
        {
            var listProfesores = new List<AsignarMateria>();
            listProfesores = _calificacionesAccesoDatos.GetIDList(filtro, cm);
            return listProfesores;
        }
    }
}
