﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class EstudiosManejador
    {
        EstudiosAccesoDatos _estudiosAccesoDatos = new EstudiosAccesoDatos();
        ProfesorAccesoDatos _profesorAccesoDatos = new ProfesorAccesoDatos();
        public void Guardar(Estudios estudios)
        {
            _estudiosAccesoDatos.Guardar(estudios);
        }

        public void Eliminar(int idusuario)
        {
            _estudiosAccesoDatos.Eliminar(idusuario);
        }

        public List<Estudios> GetEstudios(string filtro)
        {
            var listaEstudios = _estudiosAccesoDatos.GetEstudios(filtro);
            return listaEstudios;
        }

        public List<Profesores> GetProfesoresLists(ComboBox cm)
        {
            var listProfesores = new List<Profesores>();
            listProfesores = _profesorAccesoDatos.GetProfesoresLists(cm);
            return listProfesores;
        }

        public List<Profesores> GetProfesoresId(string filtro)
        {
            var listProfesores = new List<Profesores>();
            listProfesores = _profesorAccesoDatos.GetProfesoresID(filtro);
            return listProfesores;
        }

    }
}
