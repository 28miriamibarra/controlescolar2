﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class AlumnoManejador
    {
        private AlumnoAccesoDatos _alumnoAccesoDatos = new AlumnoAccesoDatos();

        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public Tuple <bool, string> ValidarAlumno(Alumno alumno)
        {
            string mensaje = "";
            bool valido = true;

            if (alumno.Nombre.Length==0)
            {
                mensaje = mensaje + "El nombre de alumno es necesario \n";
                valido = false;
            }
            else if(alumno.Nombre.Length>20)
            {
                mensaje = mensaje + "El nombre de alumno permite maximo 20 caracteres \n";
                valido = false;
            }

            else if (!NombreValido(alumno.Nombre))
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre \n";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }


        public void Guardar(Alumno alumno)
        {
            _alumnoAccesoDatos.Guardar(alumno);
        }

        public void Eliminar(int idusuario)
        {
            _alumnoAccesoDatos.Eliminar(idusuario);
        }

        public List<Alumno> GetAlumnos(string filtro)
        {
            var listalumno = _alumnoAccesoDatos.GetAlumnos(filtro);
            return listalumno;
        }

        public List<Alumno> GetNombreAlumno(ComboBox cm)
        {
            var listProfesores = new List<Alumno>();
            listProfesores = _alumnoAccesoDatos.GetNombreAlumno(cm);
            return listProfesores;
        }





    } 
}
