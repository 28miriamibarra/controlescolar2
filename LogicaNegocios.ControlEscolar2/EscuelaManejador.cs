﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class EscuelaManejador
    {
        private EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();

        public void Guardar(Escuela escuela)
        {
            _escuelaAccesoDatos.Guardar(escuela);
        }

        public void Eliminar(int idEscuela)
        {
            _escuelaAccesoDatos.Eliminar(idEscuela);
        }

        public List<Escuela> GetEscuelas(string filtro)
        {
            var listaEscuela = _escuelaAccesoDatos.GetEscuelas(filtro);
            return listaEscuela;
        }

        public DataSet  traerDatos()
        {
            DataSet x = _escuelaAccesoDatos.traerDatos();
            return x;
        }



    }
}
