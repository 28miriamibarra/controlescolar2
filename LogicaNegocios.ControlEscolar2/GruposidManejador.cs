﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class GruposidManejador
    {
        GruposidAccesoDatos _gruposidAccesoDatos = new GruposidAccesoDatos();

        public List<Gruposid> GetNombreGrupos(ComboBox cm)
        {
            var listEstados = new List<Gruposid>();
            listEstados = _gruposidAccesoDatos.GetNombreGrupos(cm);
            return listEstados;
        }

        public List<Gruposid> GetIDList(string filtro, ComboBox cm) 
        {
            var listProfesores = new List<Gruposid>();
            listProfesores = _gruposidAccesoDatos.GetIDList(filtro, cm);
            return listProfesores;
        }

        public List<Gruposid> GetGruposids(string filtro)
        {
            var listaEstudios = _gruposidAccesoDatos.GetGrupos(filtro);
            return listaEstudios;
        }

        public void Guardar(Gruposid grupo)
        {
            _gruposidAccesoDatos.Guardar(grupo);
        }

        public void Eliminar(int idgrupo)
        {
            _gruposidAccesoDatos.Eliminar(idgrupo);
        }

        public List<Gruposid> getNombre(ComboBox cm)
        {
            var listProfesores = new List<Gruposid>();
            listProfesores = _gruposidAccesoDatos.GetNombreGrupos(cm);
            return listProfesores;
        }
    }
}
