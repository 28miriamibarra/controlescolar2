﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class VistaCalificacionesManejador
    {
        private VistaCalificacionesAccesoDatos _VistaCalificacionesAccesoDatos = new VistaCalificacionesAccesoDatos();

        public List<VistaCalificaciones> GetVistas(string filtro)
        {
            var listEstados = new List<VistaCalificaciones>();
            listEstados = _VistaCalificacionesAccesoDatos.GetVistas(filtro);
            return listEstados;
        }

        public DataTable traervista(string filtro)
        {
            var dtt = _VistaCalificacionesAccesoDatos.tarervista(filtro);
            return dtt;

        }

    }

    
}
