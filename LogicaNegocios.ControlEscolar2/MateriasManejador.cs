﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class MateriasManejador
    {
        MateriasAccesoDatos _materiasAccesoDatos = new MateriasAccesoDatos();


        public void Guardar(Materias materias)
        {
            _materiasAccesoDatos.Guardar(materias);
        }

        public void Eliminar(string idmateria)
        {
            _materiasAccesoDatos.Eliminar(idmateria);
        }

        public List<Materias> GetMaterias(string filtro)
        {
            var listamaterias = _materiasAccesoDatos.GetMaterias(filtro);
            return listamaterias;
        }
        public List<Materias> GetMateriasnom(ComboBox cm)
        {
            var listProfesores = new List<Materias>();
            listProfesores = _materiasAccesoDatos.GetMateriasnom(cm);
            return listProfesores;
        }

    }
}
