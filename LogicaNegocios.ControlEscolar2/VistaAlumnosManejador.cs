﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class VistaAlumnosManejador
    {
        private VistaAlumnosAccesoDatos _vistaAlumnosAccesoDatos = new VistaAlumnosAccesoDatos();


        public List<VistaAlumnosMaterias> GetVistas()
        {
            var listEstados = new List<VistaAlumnosMaterias>();
            listEstados = _vistaAlumnosAccesoDatos.GetVistas();
            return listEstados;
        }

    }
}
