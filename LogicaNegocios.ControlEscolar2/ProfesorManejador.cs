﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Text.RegularExpressions;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class ProfesorManejador
    {
        private ProfesorAccesoDatos _profesorAccesoDatos = new ProfesorAccesoDatos();

        public void Guardar(Profesores profesor)
        {
            _profesorAccesoDatos.Guardar(profesor);
        }

        public void Eliminar(string numeroControlMaestro)
        {
            _profesorAccesoDatos.Eliminar(numeroControlMaestro);
        }

        public List<Profesores> GetProfesores(string filtro)
        {
            var listaProfesor = _profesorAccesoDatos.GetProfesores(filtro);
            return listaProfesor;
        }

        public DataSet traerUltimoNumero(string utlimoNumero)
        {
            DataSet x= _profesorAccesoDatos.traerUltimoNumero(utlimoNumero);           
            return x;
        }

       

        public DataTable ultimoNumero(string filtro)
        {
            var dtt = _profesorAccesoDatos.ultimoNumero(filtro);
            return dtt;

        }
    }
}
