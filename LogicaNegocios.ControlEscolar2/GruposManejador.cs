﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Windows.Forms;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class GruposManejador
    {
        public GruposAccesoDatos _grupoAccesoDatos = new GruposAccesoDatos();

        public void Guardar(Grupos grupos, Alumno alumno, Gruposid gruposid)
        {
            _grupoAccesoDatos.Guardar(grupos,alumno, gruposid);
        }

        public void Eliminar(int idasignar)
        {
            _grupoAccesoDatos.Eliminar(idasignar);
        }

        

        public List<Alumno> GetAlumnos(string filtro)
        {
            var listalumno = _grupoAccesoDatos.GetAlumnos(filtro);
            return listalumno;
        }

        public List<Alumno> GetIdAlumnos(string filtro, ComboBox cm)
        {
            var listalumno = _grupoAccesoDatos.GetAlumnoID(filtro,cm);
            return listalumno;
        }

        public List<Grupos> traerExistentes(string filtro, ComboBox cm)
        {
            var listalumno = _grupoAccesoDatos.traerExistentes(filtro, cm);
            return listalumno;
        }

    }
}
