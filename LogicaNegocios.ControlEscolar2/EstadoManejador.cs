﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class EstadoManejador
    {
        private EstadoAccesoDatos _estadoAccesoDatos = new EstadoAccesoDatos();

        

        public List<Estados> GetEstadosList (ComboBox cm)
        {
            var listEstados = new List<Estados>();
            listEstados = _estadoAccesoDatos.GetEstadosList(cm);
            return listEstados;
        }

        public List<Estados> CodigoEstado(string filtro)
        {
            var listEstados = new List<Estados>();
            listEstados = _estadoAccesoDatos.CodigoEstado(filtro);
            return listEstados;
        }
        
    }
}
