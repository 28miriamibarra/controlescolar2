﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Entidades.ControlEscolar2
{
    public class Gruposid
    {
        private int _idGrupo;
        private string _nombregrupo;
        private string _nomb;

        public int IdGrupo { get => _idGrupo; set => _idGrupo = value; }
        public string Nombregrupo { get => _nombregrupo; set => _nombregrupo = value; }
        public string Nomb { get => _nomb; set => _nomb = value; }
    }
}
