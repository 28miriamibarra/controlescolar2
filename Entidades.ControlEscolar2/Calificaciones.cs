﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Calificaciones
    {
        private int idcal;
        private int _Parcial1;
        private int _Parcial2;
        private int _Parcial3;
        private int _Parcial4;
        private int fkmateria;
        private int fkalumno;
        

        public int Idcal { get => idcal; set => idcal = value; }
 

        public int Fkalumno { get => fkalumno; set => fkalumno = value; }
        public int Fkmateria { get => fkmateria; set => fkmateria = value; }
        public int Parcial1 { get => _Parcial1; set => _Parcial1 = value; }
        public int Parcial2 { get => _Parcial2; set => _Parcial2 = value; }
        public int Parcial3 { get => _Parcial3; set => _Parcial3 = value; }
        public int Parcial4 { get => _Parcial4; set => _Parcial4 = value; }

    }
}
