﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Entidades.ControlEscolar2
{
    public class Usuario
    {
        private int _idusuario;
        private string _nombre;
        private string _app;
        private string _apm;
        private string _contrasenia;

        public int Idusuario { get => _idusuario; set => _idusuario = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string App { get => _app; set => _app = value; }
        public string Apm { get => _apm; set => _apm = value; }
        public string Contrasenia { get => _contrasenia; set => _contrasenia = value; }
    }
}
