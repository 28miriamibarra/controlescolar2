﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Estudios
    {
        private int _idestudio;
        private string _nombreestudio;
        private int _anoestudio;
        private string _documento;
        private int _fkprofesor;

        public int Idestudio { get => _idestudio; set => _idestudio = value; }
        public string Nombreestudio { get => _nombreestudio; set => _nombreestudio = value; }
        public int Anoestudio { get => _anoestudio; set => _anoestudio = value; }
        public int Fkprofesor { get => _fkprofesor; set => _fkprofesor = value; }
        public string Documento { get => _documento; set => _documento = value; }
    }
}
