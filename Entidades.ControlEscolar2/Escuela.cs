﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Entidades.ControlEscolar2
{
    public class Escuela
    {
        private int _idescuela;
        private string _nombrescuela;
        private string _rfcescuela;
        private string _domicilioescuela;
        private string _telefono;
        private string _email;
        private string _pagina;
        private string _nombreDirector;
        private string _logoEscuela;

        public int Idescuela { get => _idescuela; set => _idescuela = value; }
        public string Nombrescuela { get => _nombrescuela; set => _nombrescuela = value; }
        public string Rfcescuela { get => _rfcescuela; set => _rfcescuela = value; }
        public string Domicilioescuela { get => _domicilioescuela; set => _domicilioescuela = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Email { get => _email; set => _email = value; }
        public string Pagina { get => _pagina; set => _pagina = value; }
        public string NombreDirector { get => _nombreDirector; set => _nombreDirector = value; }
        public string LogoEscuela { get => _logoEscuela; set => _logoEscuela = value; }
    }
}
