﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class AsignarMateria
    {
        private int _idasigmateria;
        private string _profesor;
        private string _materia;
        private string _grupo;
        private string _semestre;

        public int Idasigmateria { get => _idasigmateria; set => _idasigmateria = value; }
        public string Profesor { get => _profesor; set => _profesor = value; }
        public string Materia { get => _materia; set => _materia = value; }
        public string Grupo { get => _grupo; set => _grupo = value; }
        public string Semestre { get => _semestre; set => _semestre = value; }
    }
}
