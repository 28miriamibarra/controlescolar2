﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class VistaCalificaciones
    {
        //private string _alumno;
        private string _materia;
        private int _parcial1;
        private int _parcial2;
        private int _parcial3;
        private int _parcial4;
        private int _id;

        //public string Alumno { get => _alumno; set => _alumno = value; }
        public string Materia { get => _materia; set => _materia = value; }
        public int Parcial1 { get => _parcial1; set => _parcial1 = value; }
        public int Parcial2 { get => _parcial2; set => _parcial2 = value; }
        public int Parcial3 { get => _parcial3; set => _parcial3 = value; }
        public int Parcial4 { get => _parcial4; set => _parcial4 = value; }
        public int Id { get => _id; set => _id = value; }
    }
}
