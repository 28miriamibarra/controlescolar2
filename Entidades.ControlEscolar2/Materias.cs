﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Materias
    {
        private int _llave;
        private string _idmateria;
        private string _nombremateria;
        private int _numhrsteoria;
        private int _numhrspractica;
        private int _creditos;
        private string _semestre;
        private string _materiaant;
        private string _materiasig;

        public int Llave { get => _llave; set => _llave = value; }
        public string Idmateria { get => _idmateria; set => _idmateria = value; }
        public string Nombremateria { get => _nombremateria; set => _nombremateria = value; }
        public int Numhrsteoria { get => _numhrsteoria; set => _numhrsteoria = value; }
        public int Numhrspractica { get => _numhrspractica; set => _numhrspractica = value; }
        public int Creditos { get => _creditos; set => _creditos = value; }
        public string Semestre { get => _semestre; set => _semestre = value; }
        public string Materiaant { get => _materiaant; set => _materiaant = value; }
        public string Materiasig { get => _materiasig; set => _materiasig = value; }
    }
}
