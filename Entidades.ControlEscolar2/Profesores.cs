﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Profesores
    {
        private int _llave;
       
        private string _numeroControlMaestro;
        private string _nombre;
        private string _apellidoPaterno;
        private string _apellidoMaterno;
        private string _direccion;
        private DateTime _fechaNac;
        private string _ciudad;
        private string _estado;
        private int _numeroCedula;
        private string titulo;

        public int Llave { get => _llave; set => _llave = value; }
       
        public string NumeroControlMaestro { get => _numeroControlMaestro; set => _numeroControlMaestro = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoPaterno { get => _apellidoPaterno; set => _apellidoPaterno = value; }
        public string ApellidoMaterno { get => _apellidoMaterno; set => _apellidoMaterno = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public DateTime FechaNac { get => _fechaNac; set => _fechaNac = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public int NumeroCedula { get => _numeroCedula; set => _numeroCedula = value; }
        public string Titulo { get => titulo; set => titulo = value; }
        
    }
}
